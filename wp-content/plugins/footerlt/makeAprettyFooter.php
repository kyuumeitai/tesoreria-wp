<?php
/**
 * Plugin Name: LT footer
 * Plugin URI: http://latercera.com
 * Description: Nueve footer's para los sitios, siete footer's para los suplementos, 3 footer's para los destacados, y un footer único para COPESA ...un footer con el poder de dominarlos a todos.
 * Version: 1.0.0
 * Author: DYD
 * Author URI: http://latercera.com
 * License: GPL2
 */

defined( 'ABSPATH' ) or die( 'No eres el que finchy' );

function LTFooter_scripts() {
    wp_register_style('footerLT-style',plugin_dir_url(__FILE__).'css/footer.css');
    wp_register_style('pluggin_style' ,plugin_dir_url(__FILE__).'css/LTfooter_pluggin.css');
    wp_enqueue_style('footerLT-style');
    wp_enqueue_style('pluggin_style');
}

add_action('get_footer','LTFooter_scripts');
/*estilos para admin*/
/* FUNCIONES Globales */
$dir = getcwd();
define('JSONDIR', $dir.'/wp-content/json');

function escribirArchivoFooter($json, $nombreArhivo) {
  $fp = fopen($nombreArhivo, 'w');
  fwrite($fp, $json);
  fclose($fp);
}
/*verifico edad del archivo*/
function isAoldMan($time,$fileUrl){
	$now = time();
  	$horaArchivo = filemtime($fileUrl);
  	if(($now - $horaArchivo) > $time){
  		return true;
  	}
  	return false;
}
/*searchAndReplaceJsonOld(int[tiempo en segundos],string[nombre archivo],json)*/
function searchAndReplaceJsonOld($tiempo,$nombreArhivo){
  if (!is_dir(JSONDIR)) {
    mkdir(JSONDIR, 0775,true);
  }
  $dirArchivo = JSONDIR."/".$nombreArhivo.".json";
  if(!file_exists($dirArchivo)){
    escribirArchivoFooter(file_get_contents('http://s1.latercera.com/wp-content/json/menuCopesa.json?date='.date("Y-m-d")), $dirArchivo);
  }else {
    if(isAoldMan($tiempo,$dirArchivo)){
      escribirArchivoFooter(file_get_contents('http://s1.latercera.com/wp-content/json/menuCopesa.json?date='.date("Y-m-d")), $dirArchivo);
    }
  }
  return json_decode(file_get_contents($dirArchivo),true);
}
//genera un archivo local para ser invocado por los dioses de wordpress
function searchAndReplaceCssOld($tiempo,$fileName){
  $url = str_replace('\\', '/', dirname(__FILE__)).'/css';
  if(!is_dir($url)){
    mkdir($url, 0775,true);
  }
  $dirArchivo = $url."/".$fileName.".css";
  if(!file_exists($dirArchivo)){
    escribirArchivoFooter(file_get_contents('http://s1.latercera.com/wp-content/themes/gnuble/css/'.$fileName.'.css?date='.date("Y-m-d")),$dirArchivo);
  }else {
    if(isAoldMan($tiempo,$dirArchivo)){
      escribirArchivoFooter(file_get_contents('http://s1.latercera.com/wp-content/themes/gnuble/css/'.$fileName.'.css?date='.date("Y-m-d")),$dirArchivo);
    }
  }
}
function searchAndReplaceImgOld($tiempo,$fileName,$ext){
  $url = str_replace('\\', '/',dirname(__FILE__)).'/img';
  if(!is_dir($url)){
    mkdir($url, 0775,true);
  }
  $dirArchivo = $url."/".$fileName.".".$ext;
  if(!file_exists($dirArchivo)){
    escribirArchivoFooter(file_get_contents('http://s1.latercera.com/wp-content/themes/gnuble/img/'.$fileName.'.'.$ext.'?date='.date("Y-m-d")),$dirArchivo);
  }else{
    if(isAoldMan($tiempo,$dirArchivo)){
      escribirArchivoFooter(file_get_contents('http://s1.latercera.com/wp-content/themes/gnuble/img/'.$fileName.'.'.$ext.'?date='.date("Y-m-d")),$dirArchivo);
    }
  }
  return $url.'/'.$fileName.'.'.$ext;
}
searchAndReplaceCssOld(86400,'footer');
searchAndReplaceCssOld(86400,'LTfooter_pluggin');
searchAndReplaceImgOld(86400,'logos-footer','png');
/*funcion que genera el footer*/
function makeAfooter(){
  $lista=searchAndReplaceJsonOld(86400,"footerLT");
	$leFooterHTML ='<div id="fullDPS"><div class="foot-logos">
						<div class="col-md-4 col-sm-4">
							<a href="http://www.grupocopesa.cl/" target="_blank" class="lfoot-gc"><img src="'.$lista["logo_copesa"].'" alt="Grupo Copesa" class="logo_position"></a>
						</div>
						<div class="col-md-8 col-sm-8 hidden-xs">
					<ul>';
    foreach ($lista["menu_medios"] as $value) {
    	$leFooterHTML .='<li><a href="'.$value["url"].'"><span class="'.$value["classes"].'"></span></a></li>';
    }
    $leFooterHTML .='</ul></div><div class="clear"></div></div></div><div class="clear"></div>';
    return $leFooterHTML;
}
/*funcion que genera informacion corporativa(?)*/
function makeCorpInfo(){
  $logoLaTercera=file_get_contents(searchAndReplaceImgOld(86400,'logo-la-tercera','svg'));
  $lista=searchAndReplaceJsonOld(86400,"footerLT");
	$corpList= '<div id="corpInfo"><nav class="noexede"><div class="logoLT lt-inv">'.$logoLaTercera.'</div><ul class="elements">';
	foreach ($lista["menu_footer"] as $value) {
    	$corpList .='<li><a href="'.$value["url"].'" target="_blank">'.$value["item"].'</a></li>';
    }
    $corpList .="</ul></nav></div><div class='clear'></div>";
    return $corpList;
}
?>