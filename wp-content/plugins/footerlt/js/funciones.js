$ = jQuery;

$(document).ready(function(){
  $("#category-adder").hide();

  $("#category-tabs > li.hide-if-no-js").hide();
  $("#category-pop").hide();
  $("#category-all").show();

  if ($('#taxonomy-category input:checkbox:checked').length > 0) {
    $('#taxonomy-category input:checkbox').not(':checked').attr("disabled", true);
  }

  $('#taxonomy-category input:checkbox').click(function(){
    var inputs = $('#taxonomy-category input:checkbox')
        if($(this).is(':checked')){
           inputs.not(this).prop('disabled',true);
        }else{
           inputs.prop('disabled',false);
        }
    });

  /*A grega preview a los custom post type WIDGETS */

  var classdydwidget = 'dyd_widget_';
  var botonPublicaroActualizar = $("#publishing-action #publish").attr("name");


  if($("body").hasClass('wp-admin') && $("body[class*='dyd_widget_']").length > 0){
    //$("#preview-action").css("opacity", 0);
    
    if($("body").hasClass('post-php')) {
      if(botonPublicaroActualizar == "save"){
        var previewLink = $("#preview-action a").attr("href");
        var urlPreview = previewLink.split(classdydwidget);
      }else if(botonPublicaroActualizar == "publish") {
        
        var previewLink = $("#sample-permalink a").text();
        var urlPreview = previewLink.split(classdydwidget);
      }
      var NewUrlPreview = urlRewrite(urlPreview[0], $_GET('post'));
      //console.log(NewUrlPreview);

      //$("#preview-action a").attr("href", NewUrlPreview);
      $("#preview-action").empty();
      $("#preview-action").append('<a target="_blank" class="preview button" href="'+NewUrlPreview+'"><span class="glyphicon glyphicon-eye-open"></span> Vista previa</a>')
    }

    if($("body").hasClass('edit-php')) { 
      $('.wp-list-table > tbody  > tr').each(function() {
        var este = $(this).find(".view a");
        var postidfromtr = $(this).attr("id");
        var postid = postidfromtr.split("post-");
        var previewLink = $(este).attr("href");
        var urlPreview = previewLink.split(classdydwidget);

        $(this).find(".view a").attr("href", urlRewrite(urlPreview[0], postid[1])).attr('target','_blank');;
      });
    }
  }

  function urlRewrite(str, postid) {
    return str+'widget-preview/?postid='+postid+'&preview='+Math.random().toString(36).substring(7);
  }

  function $_GET(param) {
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace( 
      /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
      function( m, key, value ) { // callback
        vars[key] = value !== undefined ? value : '';
      }
    );

    if ( param ) {
      return vars[param] ? vars[param] : null;  
    }
    return vars;
  }

  /*A grega preview a los custom post type WIDGETS */

  /* Cambiar render del post format STATUS */ 

  $("#post-formats-select .post-format-status").text("EspecialLT");

});
