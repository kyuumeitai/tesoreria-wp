<?php /*
Plugin Name: Avispaweb - ADD LOGO TO SITE
Plugin URI: http://www.avispaweb.com/
Description: Este plugin agrega de forma dinámica the_custom_logo() para tener mayor dominio del sitio.
Version: 1.0
Author: Marcela Godoy - Avispaweb - 2018
Author URI: http://www.avispaweb.com/
License: GPLv2
Text Domain: aw-add-logo-site
Domain Path: /languages
*/
function aw_theme_customizer_setting($wp_customize) {
// add a setting 
    $wp_customize->add_setting('logo_primary_web');
// Add a control to upload the hover logo
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_primary_web', array(
        'label' => __('Upload Logo primary','aw-add-logo-site'),
        'section' => 'title_tagline', //this is the section where the custom-logo from WordPress is
        'settings' => 'logo_primary_web',
        'priority' => 8 // show it just below the custom-logo
    )));
	// add a setting 
    $wp_customize->add_setting('logo_secundary_web');
// Add a control to upload the hover logo
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_secundary_web', array(
        'label' => __('Upload Logo Secundary','aw-add-logo-site'),
        'section' => 'title_tagline', //this is the section where the custom-logo from WordPress is
        'settings' => 'logo_secundary_web',
        'priority' => 8 // show it just below the custom-logo
    )));
}

add_action('customize_register', 'aw_theme_customizer_setting');

?>