<?php 
function archivo_scripts() {
	global $post_type;
	global $post;
	
	if ($post_type!= 'servicio')
		return;
	wp_enqueue_style( "font-awesome" );
	wp_enqueue_style( "bootstrap" );
	wp_enqueue_style( "bootstrap-theme" );
	//wp_enqueue_script( "bootstrap" );
	 //   wp_enqueue_style( 'style-name', get_stylesheet_uri() );
	wp_register_style( "css-archivo-post",AW_DIR.'/css/custom_post.css',  __FILE__  );
	wp_enqueue_style( "css-archivo-post");
	wp_register_style( "admin-css",AW_DIR.'/css/style-admin.css',  __FILE__  );
	wp_enqueue_style( "admin-css");
	



}
add_action("init", "opciones_post");
add_action( 'admin_print_scripts-post-new.php', 'archivo_scripts' );
add_action( 'admin_print_scripts-post.php', 'archivo_scripts' );
add_action( 'admin_print_scripts-post.php', 'archivo_scripts_upload' );
//add_action('admin_init',  'archivo_scripts_upload');
add_action( "wp_enqueue_scripts", "archivo_scripts_upload" );

function opciones_post(){

add_post_type_support("post", "excerpt"); // extracto en post

add_post_type_support("page", "excerpt"); // extractos para páginas

}

function wb_remove_version() {

		return "<!-- Theme Creado por MARCELA GODOY con Boostrap -->";

}


//  add_action('admin_init',  'archivo_scripts');
//add_action( "admin_enqueue_scripts", "archivo_scripts" );
function archivo_scripts_upload() {
	
global $post_type;	
	global $post;
if ($post_type!= 'servicio')
		return;
	$directorio_archivo = AW_URL_BASE_FILES.$post_type."/";
	$mes=date("m"). "/";
	$year=date("Y")."/";
	$permissions = 0755;
	$oldmask = umask( 0 );
	if ( !is_dir( $directorio_archivo ) ) mkdir( $directorio_archivo, $permissions );
	if ( !is_dir( $directorio_archivo.$year ) ) mkdir( $directorio_archivo.$year, $permissions );
	if ( !is_dir( $directorio_archivo.$year.$mes ) ) mkdir( $directorio_archivo.$year.$mes, $permissions );
	$directorio=$directorio_archivo.$year.$mes;
	$umask = umask( $oldmask );
	$chmod = chmod( $directorio, $permissions );
	
wp_register_script( "upload-file",  AW_DIR."/js/upload.js",  array("jquery"),"1.0"  );
wp_enqueue_script( "upload-file" );

wp_localize_script( "upload-file", "url",array("ruta" =>AW_DIR."/include","path"=>$directorio,"id"=>$post->ID));



}

?>