<?php 

class themes_bootstrap_aw{
	public $datos;
	public $tipo;
	public $nametipo;


	public function Collapse_targets(){
		
		?><p>
  <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Toggle first element</a>
  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2">Toggle second element</button>
  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Toggle both elements</button>
</p>
<div class="row">
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample1">
      <div class="card card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
      </div>
    </div>
  </div>
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample2">
      <div class="card card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
      </div>
    </div>
  </div>
</div><?php 
		
	}
	
	public function Acorddion(){
		
		?>
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Collapsible Group Item #1
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Collapsible Group Item #2
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
</div>

<?php
		
	}
	public function Card_Post($query=array('post_type' => 'post','order'=>'ASC','orderby'=>'date','post_status' => 'publish','posts_per_page'=> 6),$options=array('cardtype'=>'card','images'=>NULL,'url'=>NULL,'footer'=>NULL)){
		
		// agregar opcion para grupos <div class="card-group">
		// agregar opcion para grupos con separacion <div class="card-deck">
		// Card columns  Masonry-like  <div class="card-columns">
		
		/*  Cards can be organized into Masonry-like columns with just CSS by wrapping them in .card-columns. Cards are built with CSS column properties instead of flexbox for easier alignment. Cards are ordered from top to bottom and left to right.

Heads up! Your mileage with card columns may vary. To prevent cards breaking across columns, we must set them to display: inline-block as column-break-inside: avoid isn’t a bulletproof solution yet.

**********

Las tarjetas se pueden organizar en columnas tipo mampostería con solo CSS al envolverlas en .card-columns. Las tarjetas se crean con propiedades de columna CSS en lugar de flexbox para facilitar la alineación. Las tarjetas se ordenan de arriba a abajo y de izquierda a derecha.

¡Aviso! Su millaje con las columnas de la tarjeta puede variar. Para evitar que las tarjetas se rompan en las columnas, debemos configurarlas para mostrar: inline-block como column-break-inside: avoid no es una solución a prueba de balas aún.

.card-columns {
  @include media-breakpoint-only(lg) {
    column-count: 4;
  }
  @include media-breakpoint-only(xl) {
    column-count: 5;
  }
}
*/

		
		if ( !post_type_exists( $query['post_type'] ) ) {
   echo _e('<div class="alert alert-danger" role="alert">the '.$query['post_type'].' post type NO exists!!</div>');
			die;
}
		//print_r($query);
		 query_posts($query);
	// agregar opcion para grupos <div class="card-group">
		// agregar opcion para grupos con separacion <div class="card-deck">
		// Card columns  Masonry-like  <div class="card-columns">
		
		$cards=($options['cardtype'])? $options['cardtype']:"card" ;
	if($options['cardtype']) { echo '<div class="'.$cards.'">';} 	
	if ( have_posts() ) while ( have_posts() ) : the_post(); 
 		global $post;
		$custom = get_post_custom( $post->ID );
		
 ?>
<div class="card <?php echo $options['width_class'];?>">

  <div class="card-body">
	   <?php 

		if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) {
			the_post_thumbnail($options['images']['size'],array('class'=>$options['images']['class']));
		} else {
			//echo '<img class="card-img-top" src="'.$image0.'" alt="'.get_the_title().'">';
		}
	

 ?>
    <h5 class="card-title"><?php the_title(); ?></h5>
    <p class="card-text"><?php the_excerpt(); ?></p>
  <?php if($options['btn']) { echo '<a href="'.get_the_permalink().'" class="'.$options['btn']['class'].'">'.$options['btn']['title'].'</a>'; }?>
  </div>
	
	<!-- OPCIONAL-->
	<?php if($options['footer']) { echo '
	<div class="card-footer">
      <small class="text-muted">Last updated 3 mins ago</small>
    </div>';
				}
	?>
</div>
 <?php 
 
	  endwhile; 
		wp_reset_postdata();
	if($options['cardtype']) { echo "</div>";} 
 ?>

<?php
	}

	public function Image_cap(){
		
		?>
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src=".../100px180/?text=Image cap" alt="Card image cap">
  <div class="card-body">
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<?php
	}
	
}

?>