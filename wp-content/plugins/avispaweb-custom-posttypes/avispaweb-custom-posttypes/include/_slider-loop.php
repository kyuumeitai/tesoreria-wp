<div id="slider-home" class="carousel slide " data-ride="carousel">
  <div class="carousel-inner" role="listbox">

		<?php $i=0;
	$efectos=array("flipInX","bounceInRight","bounceInLeft","flipInY", "bounceInUp","bounceIn","bounceInDown","fadeInDownShort","fadeInUpShort","shake","shakeUp","wiggle","swing","pulse");

	$sql= array( 'post_type' => 'slider','order' => 'DESC','orderby'=>'date','post_status' => 'publish',
				'tipo_slider'  => array( $category ),'showposts' => -1);
query_posts($sql);
		if (have_posts()) :	while (have_posts()) : the_post();
	global $post;
	$custom = get_post_custom( $post->ID );
		if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) {
			$image_attributes = wp_get_attachment_image_src ( get_post_thumbnail_id ( $post->ID), 'full' ) ; 
			$image= $image_attributes[0];
		}
	$links= (@$custom["slider_url"][0]!=="")? @$custom["slider_url"][0]:""; 
	$boton_name= (@$custom["slider_btn"][0]!=="")? @$custom["slider_btn"][0]:"Más Información"; 
	$target= (@$custom["slider_url_target"][0]!=="")? @$custom["slider_url_target"][0]:"_self"; 
	$video=(@$custom["slider_video_url"][0]!=="")? @$custom["slider_video_url"][0]:"";
	$videoimg=(@$custom["slider_video_img"][0]!=="")? @$custom["slider_video_img"][0]:"";
	?>
			<?php  
		
		
		if (!$video){ $imageid = MultiPostThumbnails::get_post_thumbnail_id(get_post_type(), 'background-slider', $post->ID);
$imageurl = wp_get_attachment_image_src($imageid,'large'); 
					 $imagen=($imageurl[0]!=="")? $imageurl[0]:"none";
					}?>
			   <div class="carousel-item <?php if ($i == 0) echo ' active '; ?>" >
                    <img class="bg-image" src="<?php echo $imageurl[0];?>" alt="<?php the_title();?>">
                    <div class="container-fluid h-100 py-5">
						<div class="row align-items-center h-100">
						<div class="col-lg-8 col-xl-6 mx-auto text-white text-center">
						<?php the_content();?></div>
						</div>
                      
                    </div>
                  </div>		
		<?php /*/			
		<div class="carousel-item  animated <?php if ($i == 0) echo ' active '; ?>" data-id="<?php echo $i+1;?>" style="background-image: url(<?php echo $imageurl[0];?>);" class="bg">
		
	<?php	if ($video){?><video poster="<?php echo $videoimg;?>" id="video-<?php echo $i;?>" class="bgvid" playsinline autoplay muted >
  <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
<source src="<?php echo $video;?>" type="video/mp4">
</video>
	<div class="playvideo" data-video="#video-<?php echo $i;?>" title="Video Play/Pause"><i class="fa fa-pause"></i></div><?php } // fin de video ?>
		
	<div class="bg">
				<div class="carousel-caption"><div class="text parallax-scrolling animated delay-250 <?php echo $efectos[rand(0,count($efectos))]; ?>">
						<h3><?php the_title();?></h3><span class=""><?php echo  str_replace(array("<p>","</p>"), "", get_the_excerpt());?></span>
					<?php if($links!=="") {?><a href="<?php echo $links;?>" class="btn btn btn-warning"><?php echo $boton_name;?></a><?php } ?>
					</div>
					
					</div>
					</div>
				
					
		
			</div> */?>
		<?php  $i++; endwhile; endif; wp_reset_query();  ?>

	</div>
	
	<!-- Controls -->
	<a class="carousel-control-prev" href="#slider-home" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#slider-home" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> <span class="sr-only">Next</span> </a> 
<ol class="carousel-indicators">
	<?php for ($a=0;$a<$i;$a++) { ?>
	<li data-target="#slider-home" data-slide-to="<?php echo $a;?>" class="<?php if ($a == 0) echo 'active'; ?>"></li>
	<?php } ?>
</ol>


</div>