<h2 style="font-size: 2em;"><?php echo __('Slider Image Full', 'textdomain' );?></h2>

<h3><?php echo __('Enlace del Slider (Opcional)', 'textdomain' );?></h3>
	<table width="100%"  border="0" cellpadding="5" cellspacing="0" class="table-style">
<tr>
<td width="13%" align="left">URL: </td>
<td width="87%" align="left"><input type="text" name="slider_url" id="slider_url" value="<?php echo @$custom["slider_url"][0]; ?>"  size="30"  placeholder="http://" /></td>
</tr>
<tr>
<td align="left"><?php echo __('Abrir el enlace', 'textdomain' );?></td>
<td align="left"><select id="slider_url_target" name="slider_url_target">
	<option value=""><?php echo __('Selecione opción', 'textdomain' );?></option>
	<option value="_self" selected="selected" <?php echo (@$custom["slider_url_target"][0]=="_self")? "selected":"" ;?>><?php echo __('Abrir en la misma Ventana', 'textdomain' );?></option> 
	<option value="_blank" <?php echo (@$custom["slider_url_target"][0]=="_blank")? "selected":"" ;?>><?php echo __('Abrir en ventana Nueva', 'textdomain' );?></option> </select></td>
</tr>
<tr>
<td width="13%" align="left">Texto en Boton: </td>
<td width="87%" align="left"><input type="text" name="slider_btn" id="slider_btn" value="<?php echo @$custom["slider_btn"][0]; ?>"  size="30"  placeholder="Ejemplo: Más Información" /></td>
</tr>
</table>
