<?php 
/*
Plugin Name: Avispaweb - Custom Post
Plugin URI: http://www.avispaweb.com/
Description: Este plugin añade diferentes funcionalidades al sitio. I18n
Version: 2.2
Author: Marcela Godoy - Avispaweb
Author URI: http://www.avispaweb.com/
License: GPLv2

Version 2.1 cambios
- Se agrega AW_URL_BASE_FILES con variable dinamica según Carpeta de usuario/sitio
# define('AW_URL_BASE_FILES', '/home2/avispaweb/andinatm/wp-content/uploads/'); 
define('AW_URL_BASE_FILES', $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/'); 
- Se agrega PostType BLOG + author $supportsBLOG  -  $argsBLOG
- Se agrega PostType Clientes 
- Se agrega Categoría a Servicios, Clientes, Blog
 
*/
error_reporting(E_ALL);
if ( !defined('AW_DIR') ) define('AW_DIR',  '/wp-content/plugins/'.plugin_basename( dirname(__FILE__)) );
define('AW_URL_BASE_FILES', $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/'); 

//require_once("plugin/plugin.php");
require_once('include/Create_Post_Types.php');


$supportsRemate =array( 'title');

$argsRemate=array('public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,'show_in_rest'=>true,
		'query_var' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'menu_position' => 3,
		'supports' => $supportsRemate,
		'menu_icon' => 'dashicons-format-gallery');

/*
	"agnoExpJud":2018;
	"codDemanda":1532750; 
	"comunaJuzgado":"SANTIAGO";
	"direccionJuzgado":"Teatinos 28";
	"direccionRol":"MAIPU 25";
	"fechaActualizacion":"2018-10-22T16:36:42-03:00";
	"fechaPrimeraPublicacion":"2018-11-14T00:00:00-03:00";
	"fechaRemate":"2019-02-22T10:00:00-03:00";
	"idComunaJuzgado":13101;
	"idJuzgado":4527494;
	"idTipoDeuda":1;
	"identificacionExpedienteAdm":"514-2005-SANTIAGO";
	"marcaPrimeraPublicacion":"X";
	"medioPublicacion":"Emol";
	"nombreDuegno":"ALEJANDRO ANTONIO SALINAS CELIS";
	"nombreJuzgado":"15° JUZGADO CIVIL DE SANTIAGO";
	"nroExpJud":1021;
	"orden":1;
	"pblRemId":48;
	"periodoPublicacionF":"06-2018";
	"periodoPublicacionI":"05-2012";
	"rol":"100100002";
	"rolFormato":"ARICA-00100-002";
	"rutDuegno":"8.313.031-8";
	"tasacion":7348036;
	"tipoDeuda":"FISCAL";
	"tipoImpuesto":"Impuesto a la Renta";
*/

global $rematesVariables;
if(empty($rematesVariables)){
$rematesVariables=array('agnoExpJud', 'codDemanda', 'comunaJuzgado', 'direccionJuzgado', 'direccionRol', 'fechaActualizacion', 'fechaPrimeraPublicacion', 'fechaRemate', 'idComunaJuzgado', 'idJuzgado', 'idTipoDeuda', 'identificacionExpedienteAdm', 'marcaPrimeraPublicacion', 'medioPublicacion', 'nombreDuegno', 'nombreJuzgado', 'nroExpJud', 'orden', 'pblRemId', 'periodoPublicacionF', 'periodoPublicacionI', 'rol', 'rolFormato', 'rutDuegno', 'tasacion', 'tipoDeuda', 'tipoImpuesto');

}
$remates= new AW_Create_Post_Types('remate','remates',$argsRemate);

require_once('include/_post_types-remate.php');
require_once('include/query_post_types.php');

function Create_category_init($name,$plural,$postname) {

	$args = array (
		'label' => esc_html__( 'Categorías de '.$name, 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Categorías de '.$name, 'text-domain' ),
			'all_items' => esc_html__( 'All Categorías de '.$name, 'text-domain' ),
			'edit_item' => esc_html__( 'Edit Categoría de '.$name, 'text-domain' ),
			'view_item' => esc_html__( 'View Categoría de '.$name, 'text-domain' ),
			'update_item' => esc_html__( 'Update Categoría de '.$name, 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new Categoría de '.$name, 'text-domain' ),
			'new_item_name' => esc_html__( 'New Categoría de '.$name, 'text-domain' ),
			'parent_item' => esc_html__( 'Parent Categoría de '.$name, 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent Categoría de Biblioteca:', 'text-domain' ),
			'search_items' => esc_html__( 'Search Categorías de '.$plural, 'text-domain' ),
			'popular_items' => esc_html__( 'Popular Categorías de '.$plural, 'text-domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Categorías de '.$plural.' with commas', 'text-domain' ),
			'add_or_remove_items' => esc_html__( 'Add or remove Categorías de '.$plural, 'text-domain' ),
			'choose_from_most_used' => esc_html__( 'Choose most used Categorías de '.$plural, 'text-domain' ),
			'not_found' => esc_html__( 'No Categorías de '.$plural.' found', 'text-domain' ),
			'name' => esc_html__( 'Categorías de '.$plural, 'text-domain' ),
			'singular_name' => esc_html__( 'Categoría de '.$plural, 'text-domain' ),
		),
		'public' => true,
		'show_ui'=> true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	'show_in_rest' => true,
		'rest_base' => false,
	'hierarchical' => true,
		'sort' => true,
		
	);

	register_taxonomy( $name, array( $postname ), $args );
	flush_rewrite_rules();
}



function category_not__slug($post,$tax) {

	$args = array (
		'label' => esc_html__( 'Categorías de ' . $post , 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Categorías de ' . $post , 'text-domain' ),
			'all_items' => esc_html__( 'All Categorías de ' . $post , 'text-domain' ),
			'edit_item' => esc_html__( 'Edit Categoría de ' . $post , 'text-domain' ),
			'view_item' => esc_html__( 'View Categoría de ' . $post , 'text-domain' ),
			'update_item' => esc_html__( 'Update Categoría de ' . $post , 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new Categoría de ' . $post , 'text-domain' ),
			'new_item_name' => esc_html__( 'New Categoría de ' . $post , 'text-domain' ),
			'parent_item' => esc_html__( 'Parent Categoría de ' . $post , 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent Categoría de ' . $post .':', 'text-domain' ),
			'search_items' => esc_html__( 'Search Categorías de ' . $post , 'text-domain' ),
			'popular_items' => esc_html__( 'Popular Categorías de ' . $post , 'text-domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Categorías de ' . $post .' with commas', 'text-domain' ),
			'add_or_remove_items' => esc_html__( 'Add or remove Categorías de ' . $post , 'text-domain' ),
			'choose_from_most_used' => esc_html__( 'Choose most used Categorías de ' . $post , 'text-domain' ),
			'not_found' => esc_html__( 'No Categorías de ' . $post .' found', 'text-domain' ),
			'name' => esc_html__( 'Categorías de ' . $post, 'text-domain' ),
			'singular_name' => esc_html__( 'Categoría de ' . $post , 'text-domain' ),
		),
		'public' => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	'show_in_rest' => true,
	//	'rest_base' => false,
	'hierarchical' => true,
		'sort' => true,
		
	);

	register_taxonomy( $tax, array( $post ), $args );
	flush_rewrite_rules();
}