<?php 
global $datosTitulosRemate;
$datosTitulosRemate=array("Fecha de subasta","Tribunal que efectuará el remate","Dirección del Tribunal","Rol de la causa judicial" ,"Expediente(s) administrativo(s) que dieron origen al Expediente Judicial", "Tipo de impuesto que se cobra","Periodo del impuesto que se cobra", "Inscripción del inmueble","Fecha primera publicación","Fecha segunda publicación", "Nombre del demandado",
	 	"RUT del demandado","Dirección del inmueble", "Rol de avalúo fiscal del inmueble", "Tasación del inmueble","Otros Datos");
function add_meta_boxes_archivo() {

	add_meta_box( "add_archivo_adjunto", "Documento Adjunto", "add_archivo_adjunto", "remate", "side", "high" );
}
function add_archivo_adjunto() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$tipo_post = get_post_type( $post->ID );
	$datos = new load_posttypes();
	?>
	<div class="archivo_archivo">
		<?php if ( isset($custom["archivo-adjunto"][0])): 	
$archivo=$custom["archivo-adjunto"][0];
$datos->show_descarga($archivo);
 endif; ?>
		<div class="file-upload">
			<label for="file" class="file-upload__label button-primary">ADJUNTAR <?php if ( isset($custom["file_archivo"][0] )): ?>NUEVO <?php endif; ?>ARCHIVO</label>
			<input id="file" class="file-upload__input" type="file" name="file">
		</div><br/>
		<small>Subida máx. de archivo: <?php echo  ini_get('upload_max_filesize');?></small>
		<div id="respuesta" class="alert"></div>
		<progress id="barra_de_progreso" value="0" max="100"></progress>
		<div id="message_file"></div>
	</div>
		<input type="hidden" id="archivo-adjunto" name="archivo-adjunto" value="<?php echo (isset($custom["archivo-adjunto"][0]))? @$custom["archivo-adjunto"][0]:" ";?>">

	
		<?php // <h3>Archivos subidos anteriormente</h3>
	//$datos->lista_archivos_adjuntos($post->ID);
	?>

	<?php 

}
function save_archivo() {
	global $post;
	global $post_type;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post->ID;
	}
	  if ( isset($post->ID) && $post_type=== 'remate') {	
		if ( isset($_POST[ "archivo-adjunto" ] ) ) {
		update_post_meta( $post->ID, "archivo-adjunto", @$_POST[ "archivo-adjunto" ] );
		
		}
	  } 
	
}



add_action( 'add_meta_boxes', 'add_meta_boxes_datos_remates' );
add_action( 'save_post', 'save_datos_remates' );
add_action('publish_post', 'save_datos_remates');

function add_meta_boxes_datos_remates() {
add_meta_box("datos_remates", "Información del Remate", "add_datos_remate","remate", "normal", "high");
}

function add_datos_remate() {
	global $post,$datosTitulosRemate;
	$custom = get_post_custom( $post->ID );
?><h3>Información del Remate</h3>
<ul>
<?php

	/* 
•	Fecha de subasta: corresponde a la Fecha de Remate. 
•	Tribunal que efectuará el remate. 
•	Dirección del Tribunal. 
•	Rol de la causa judicial. 
•	Expediente(s) administrativo(s) que dieron origen al Expediente Judicial. 
•	Tipo de impuesto que se cobra. 
•	Periodo del impuesto que se cobra. 
•	Inscripción del inmueble (optativo para el impuesto territorial). 
•	Fecha primera publicación / Fecha segunda publicación. (según corresponda). 
•	Nombre del demandado.
•	RUT del demandado (optativo para el impuesto territorial). 
•	Dirección del inmueble. 
•	Rol de avalúo fiscal del inmueble. 
•	Tasación del inmueble. 
•	Y, eventualmente, otra información que la ley o TGR disponga.

	*/


		for($i=0;$i<count($datosTitulosRemate);$i++) { ?>
<li>
<label for="datos_remate<?php echo $i;?>"><?php echo $datosTitulosRemate[$i];?></label>
<input type="text" name="datos_remate<?php echo $i;?>" id="datos_remate<?php echo $i;?>" value="<?php echo @$custom["datos_remate".$i][0]; ?>"/>	</li> 
<?php } ?>
	</table>

<?php 

}

function save_datos_remates(){

  global $post;
	global $post_type;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post->ID;
	}

   if ( isset($post->ID) && $post_type=== 'remate') {	

	 $post_id = $post->ID;	 
  	for($i=0;$i<count($datosTitulosRemate);$i++) {
	if (@$_POST["datos_remate".$i]!=="" ){ update_post_meta($post_id, 'datos_remate'.$i, @$_POST["datos_remate".$i], false); } 
		}
  }

}


?>