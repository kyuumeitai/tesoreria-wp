<?php
  function custom_dashboard_widget_manual() { ?>
            
            <h1><?php echo 'Manual de Uso ';?> </h1>
            <ul style="list-style-type:upper-roman ; padding-left: 15px;">
            	<li><a href="/wp-content/uploads/2018/01/MANUAL-DE-USO.pdf" target="_blank">Conociendo Wordpress</a></li>
          
            	
            </ul>
    <?php }   function custom_dashboard_widget() { 
      $custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
if ( has_custom_logo() ) { ?>
<img src="<?php echo  esc_url( $logo[0] ); ?>" title="<?php bloginfo( 'name' );?>" style="max-width: 100%; width: 50px">
	
   <?php
}   ?>
            <h1>Bienvenidos al Administrador de  <?php  bloginfo( 'name' );?></h1>
            <p>Próximamente, agregaremos el contenido de ayuda para administrar tu Web.</p>
            <p>Que tengas un éxitoso día.</p>
            <p>Atte. Equipo de Desarrollo AvispaWeb</p>
    <?php } 
    
    add_action( 'wp_dashboard_setup', 'my_dashboard_setup_function' );
 
function my_dashboard_setup_function() {
    add_meta_box( 'my_dashboard_widget', 'Bienvenidos a la administración de '.get_bloginfo( 'name' ), 'custom_dashboard_widget', 'dashboard', 'normal', 'high' );
    add_meta_box( 'my_dashboard_manual', 'Manual de Uso '.get_bloginfo( 'name' ), 'custom_dashboard_widget_manual', 'dashboard', 'side', 'high' );
}

// Eliminamos metaboxes del Dashboard
 
    function example_remove_dashboard_widgets() {
            remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
           remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );       
            remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );     
            remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );               
            remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   
            remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' ); 
            remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal' );    
		remove_action('welcome_panel', 'wp_welcome_panel');
    } 
 
    add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets' ); 
?>