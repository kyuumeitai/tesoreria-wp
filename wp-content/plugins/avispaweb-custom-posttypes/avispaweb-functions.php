<?php 
/*
Plugin Name: Avispaweb - Custom Post
Plugin URI: http://www.avispaweb.com/
Description: Este plugin añade diferentes funcionalidades al sitio. I18n
Version: 2.2
Author: Marcela Godoy - Avispaweb
Author URI: http://www.avispaweb.com/
License: GPLv2

Version 2.1 cambios
- Se agrega AW_URL_BASE_FILES con variable dinamica según Carpeta de usuario/sitio
# define('AW_URL_BASE_FILES', '/home2/avispaweb/andinatm/wp-content/uploads/'); 
define('AW_URL_BASE_FILES', $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/'); 

*/
//error_reporting(E_ALL);

error_reporting(E_ERROR | E_WARNING );
if ( !defined('AW_DIR') ) define('AW_DIR',  '/wp-content/plugins/'.plugin_basename( dirname(__FILE__)) );
define('AW_URL_BASE_FILES', $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/'); 

require_once("plugin/plugin.php");
require_once('include/Create_Post_Types.php');
global $image0;
$image0="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcyIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MiAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTYxMjllNTUzZDUgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNjEyOWU1NTNkNSI+PHJlY3Qgd2lkdGg9IjE3MiIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OC45NTgzMzM5NjkxMTYyMSIgeT0iOTQuOTUiPjE3MngxODA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=";
$supports =array( 'title', 'page-attributes' );
$supportsBLOG =array( 'title', 'thumbnail', 'excerpt', 'editor','author' );
$args=array('public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'hierarchical' => true,'show_in_rest'=>true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'menu_position' => 3,
		'supports' => $supports,
		'menu_icon' => 'dashicons-format-gallery',);

$args_nopublic=array('public' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,'show_in_rest'=>false,
        'show_ui' => true,
		'show_in_menu' => true,
        'capability_type' => 'post','has_archive' => false,
        'hierarchical' => true,
        'rewrite' => array( 'with_front' => false ),
        'query_var' => true,
        'supports' => $supports,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-camera');
$args_admin=array('public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_menu' => 'edit.php?post_type=remate',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'archivo-adjunto' ),
		'capability_type' => [ 'archivo-adjunto', 'archivos-adjuntos' ],
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'supports' => array( 'title' ),
		'menu_position' => 3,
		'menu_icon' => 'dashicons-archive',);
//unregister_post_type('plan');
$servicio= new AW_Create_Post_Types('remate','remates',$args);
require_once('include/_post_types-remates.php');
require_once('include/query_post_types.php');


add_theme_support( 'post-thumbnails' );

add_action( 'init', 'sk_add_category_taxonomy_to_events' );
function sk_add_category_taxonomy_to_events() {
	//register_taxonomy_for_object_type( 'category', 'servicio' );
	//register_taxonomy_for_object_type( 'servicios', 'servicio' );
	//category_not__slug('servicio','tipo_servicio');
	//category_not__slug('faq','categoria');
	//category_not__slug('blog','category_blog');
	//Create_category_init('category_blog','category_blogs','blog');
	//Create_category_init('tipo_servicio','tipo_servicios','servicio');

}



function Create_category_init($name,$plural,$postname) {

	$args = array (
		'label' => esc_html__( 'Categorías de '.$name, 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Categorías de '.$name, 'text-domain' ),
			'all_items' => esc_html__( 'All Categorías de '.$name, 'text-domain' ),
			'edit_item' => esc_html__( 'Edit Categoría de '.$name, 'text-domain' ),
			'view_item' => esc_html__( 'View Categoría de '.$name, 'text-domain' ),
			'update_item' => esc_html__( 'Update Categoría de '.$name, 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new Categoría de '.$name, 'text-domain' ),
			'new_item_name' => esc_html__( 'New Categoría de '.$name, 'text-domain' ),
			'parent_item' => esc_html__( 'Parent Categoría de '.$name, 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent Categoría de Biblioteca:', 'text-domain' ),
			'search_items' => esc_html__( 'Search Categorías de '.$plural, 'text-domain' ),
			'popular_items' => esc_html__( 'Popular Categorías de '.$plural, 'text-domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Categorías de '.$plural.' with commas', 'text-domain' ),
			'add_or_remove_items' => esc_html__( 'Add or remove Categorías de '.$plural, 'text-domain' ),
			'choose_from_most_used' => esc_html__( 'Choose most used Categorías de '.$plural, 'text-domain' ),
			'not_found' => esc_html__( 'No Categorías de '.$plural.' found', 'text-domain' ),
			'name' => esc_html__( 'Categorías de '.$plural, 'text-domain' ),
			'singular_name' => esc_html__( 'Categoría de '.$plural, 'text-domain' ),
		),
		'public' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	'show_in_rest' => true,
		'rest_base' => false,
	'hierarchical' => true,
		'sort' => true,
		
	);

	register_taxonomy( $name, array( $postname ), $args );
	flush_rewrite_rules();
}

add_action('save_post', 'assign_parent_terms', 10, 2);

function assign_parent_terms($post_id, $post){
/* selecciona las categorias superiores */
global $post;

if(isset($post) && $post->post_type != 'blog')
return $post_id;

    // get all assigned terms   
    $terms = wp_get_post_terms($post_id, 'category_blog' );
    foreach($terms as $term){
        while($term->parent != 0 && !has_term( $term->parent, 'category_blog', $post )){
            // move upward until we get to 0 level terms
            wp_set_post_terms($post_id, array($term->parent), 'category_blog', true);
            $term = get_term($term->parent, 'category_blog');
        }
    }
}

function cpt_slug_template_avispa($template) {
  global $post;
 //echo "<!-- ". $post->ID." -->";
  $templ = locate_template('single-'.$post->post_type.'-ID-'.$post->ID.'.php');
  if (
    'servicio' == $post->post_type 
  
    &&  !empty($templ)) {
      $template = get_stylesheet_directory().'/single-'.$post->post_type.'-ID-'.$post->ID.'.php';
	 // echo "<!-- ". $template." -->";
  }

  return $template;
}
add_filter('single_template','cpt_slug_template_avispa');
function category_not__slug($post,$tax) {

	$args = array (
		'label' => esc_html__( 'Categorías de ' . $post , 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Categorías de ' . $post , 'text-domain' ),
			'all_items' => esc_html__( 'All Categorías de ' . $post , 'text-domain' ),
			'edit_item' => esc_html__( 'Edit Categoría de ' . $post , 'text-domain' ),
			'view_item' => esc_html__( 'View Categoría de ' . $post , 'text-domain' ),
			'update_item' => esc_html__( 'Update Categoría de ' . $post , 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new Categoría de ' . $post , 'text-domain' ),
			'new_item_name' => esc_html__( 'New Categoría de ' . $post , 'text-domain' ),
			'parent_item' => esc_html__( 'Parent Categoría de ' . $post , 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent Categoría de ' . $post .':', 'text-domain' ),
			'search_items' => esc_html__( 'Search Categorías de ' . $post , 'text-domain' ),
			'popular_items' => esc_html__( 'Popular Categorías de ' . $post , 'text-domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Categorías de ' . $post .' with commas', 'text-domain' ),
			'add_or_remove_items' => esc_html__( 'Add or remove Categorías de ' . $post , 'text-domain' ),
			'choose_from_most_used' => esc_html__( 'Choose most used Categorías de ' . $post , 'text-domain' ),
			'not_found' => esc_html__( 'No Categorías de ' . $post .' found', 'text-domain' ),
			'name' => esc_html__( 'Categorías de ' . $post, 'text-domain' ),
			'singular_name' => esc_html__( 'Categoría de ' . $post , 'text-domain' ),
		),
		'public' => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	'show_in_rest' => true,
	//	'rest_base' => false,
	'hierarchical' => true,
		'sort' => true,
		
	);

	register_taxonomy( $tax, array( $post ), $args );
	flush_rewrite_rules();
}

require_once("include/dashboard.php");