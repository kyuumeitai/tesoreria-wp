<?php 
/*
Plugin Name: Avispaweb - Custom Post
Plugin URI: http://www.avispaweb.com/
Description: Este plugin añade diferentes funcionalidades al sitio. I18n
Version: 2.2
Author: Marcela Godoy - Avispaweb
Author URI: http://www.avispaweb.com/
License: GPLv2

Version 2.1 cambios
- Se agrega AW_URL_BASE_FILES con variable dinamica según Carpeta de usuario/sitio
# define('AW_URL_BASE_FILES', '/home2/avispaweb/andinatm/wp-content/uploads/'); 
define('AW_URL_BASE_FILES', $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/'); 
- Se agrega PostType BLOG + author $supportsBLOG  -  $argsBLOG
- Se agrega PostType Clientes 
- Se agrega Categoría a Servicios, Clientes, Blog
 
*/
error_reporting(E_ERROR | E_WARNING );
error_reporting(E_ALL );
if ( !defined('AW_DIR') ) {
define('AW_DIR',  '/wp-content/plugins/'.plugin_basename( dirname(__FILE__)) );
define('AW_URL_BASE_FILES', $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/'); 


}
//require_once("plugin/plugin.php");
require_once('include/Create_Post_Types.php');


$supportsRemate =array( 'title');

$argsRemate=array('public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,'show_in_rest'=>true,
		'query_var' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'menu_position' => 3,
		'supports' => $supportsRemate,
		'menu_icon' => 'dashicons-format-gallery');

$argsPub=array('public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,'show_in_rest'=>true,
		'query_var' => true,
		'has_archive' => 'publicacion',

		'rewrite' => array( 'with_front' => true , ),
		'hierarchical' => false,
		'exclude_from_search' => false,
		'menu_position' => 3,
		'supports' => $supportsRemate,
		'menu_icon' => 'dashicons-format-gallery');



global $rematesVariables;



$remates= new AW_Create_Post_Types('remate','remates',$argsRemate);
$publicaciones= new AW_Create_Post_Types('publicacion','publicaciones',$argsPub);
add_action( 'init', 'sk_add_category_taxonomy_to_events' );
function sk_add_category_taxonomy_to_events() {


	/* genera categorias fantasmas, sin visibilidad de slug, ideal para categorizar */
	category_not__slug('remate','comuna');
	category_not__slug('remate','idpublicacion');
	category_not__slug('remate','vigente');
	category_not__slug('remate','tribunal');
	category_not__slug('publicacion','comuna');
	category_not__slug('publicacion','tribunal');
	register_taxonomy_for_object_type( 'comuna', 'publicacion' );
	register_taxonomy_for_object_type( 'tribunal', 'publicacion' );
	register_taxonomy_for_object_type( 'comuna', 'remate' );
	register_taxonomy_for_object_type( 'idpublicacion', 'remate' );
	register_taxonomy_for_object_type( 'vigente', 'remate' );
	register_taxonomy_for_object_type( 'tribunal', 'remate' );
	// Create_category_init('category_blog','category_blogs','blog');



}



require_once('include/_post_types-remate.php');
require_once('include/query_post_types.php');
require_once('include/regiones.php'); 
require_once('include/tribunales.php'); 


add_action('admin_menu','add_menu_AW_importar_datos');	

function add_menu_AW_importar_datos() {

add_submenu_page( 'edit.php?post_type=remate', 'Importar Datos', 'Importar Datos',  ('administrator'), 'AW_importar_datos', 'AW_importar_datos__Form' ); 
add_submenu_page( 'edit.php?post_type=publicacion', 'Importar Datos', 'Importar Datos', ('administrator'), 'AW_importar_datos_P', 'AW_importar_datos__Form' ); 
}


function AW_importar_datos__Form( ){


require_once( 'conexion-ws.php'); 
}



function Create_category_init($name,$plural,$postname) {

	$args = array (
		'label' => esc_html__( 'Categorías de '.$name, 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( 'Categorías de '.$name, 'text-domain' ),
			'all_items' => esc_html__( 'All Categorías de '.$name, 'text-domain' ),
			'edit_item' => esc_html__( 'Edit Categoría de '.$name, 'text-domain' ),
			'view_item' => esc_html__( 'View Categoría de '.$name, 'text-domain' ),
			'update_item' => esc_html__( 'Update Categoría de '.$name, 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new Categoría de '.$name, 'text-domain' ),
			'new_item_name' => esc_html__( 'New Categoría de '.$name, 'text-domain' ),
			'parent_item' => esc_html__( 'Parent Categoría de '.$name, 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent Categoría de Biblioteca:', 'text-domain' ),
			'search_items' => esc_html__( 'Search Categorías de '.$plural, 'text-domain' ),
			'popular_items' => esc_html__( 'Popular Categorías de '.$plural, 'text-domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Categorías de '.$plural.' with commas', 'text-domain' ),
			'add_or_remove_items' => esc_html__( 'Add or remove Categorías de '.$plural, 'text-domain' ),
			'choose_from_most_used' => esc_html__( 'Choose most used Categorías de '.$plural, 'text-domain' ),
			'not_found' => esc_html__( 'No Categorías de '.$plural.' found', 'text-domain' ),
			'name' => esc_html__( 'Categorías de '.$plural, 'text-domain' ),
			'singular_name' => esc_html__( 'Categoría de '.$plural, 'text-domain' ),
		),
		'public' => true,
		'show_ui'=> true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	'show_in_rest' => true,
		'rest_base' => false,
	'hierarchical' => true,
		'sort' => true,
		
	);

	register_taxonomy( $name, array( $postname ), $args );
	flush_rewrite_rules();
}



function category_not__slug($post,$tax) {

	$args = array (
		'label' => esc_html__( '' . ucfirst($tax) , 'text-domain' ),
		'labels' => array(
			'menu_name' => esc_html__( '' . ucfirst($tax) , 'text-domain' ),
			'all_items' => esc_html__( 'All ' . ucfirst($tax) , 'text-domain' ),
			'edit_item' => esc_html__( 'Edit  ' . ucfirst($tax) , 'text-domain' ),
			'view_item' => esc_html__( 'View  ' . ucfirst($tax) , 'text-domain' ),
			'update_item' => esc_html__( 'Update  ' . ucfirst($tax) , 'text-domain' ),
			'add_new_item' => esc_html__( 'Add new  ' . ucfirst($tax) , 'text-domain' ),
			'new_item_name' => esc_html__( 'New  ' . ucfirst($tax) , 'text-domain' ),
			'parent_item' => esc_html__( 'Parent  ' . ucfirst($tax) , 'text-domain' ),
			'parent_item_colon' => esc_html__( 'Parent  ' . ucfirst($tax) .':', 'text-domain' ),
			'search_items' => esc_html__( 'Search ' . ucfirst($tax) , 'text-domain' ),
			'popular_items' => esc_html__( 'Popular ' . ucfirst($tax) , 'text-domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate ' . ucfirst($tax) .' with commas', 'text-domain' ),
			'add_or_remove_items' => esc_html__( 'Add or remove ' . ucfirst($tax) , 'text-domain' ),
			'choose_from_most_used' => esc_html__( 'Choose most used ' . ucfirst($tax) , 'text-domain' ),
			'not_found' => esc_html__( 'No ' . ucfirst($tax) .' found', 'text-domain' ),
			'name' => esc_html__( '' . ucfirst($tax), 'text-domain' ),
			'singular_name' => esc_html__( ' ' . ucfirst($tax) , 'text-domain' ),
		),
		'public' => false,
		'show_ui'           => false,
		'show_admin_column' => true,
		'query_var'         => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	'show_in_rest' => true,
	//	'rest_base' => false,
	'hierarchical' => true,
		'sort' => true,
		
	);

	register_taxonomy( $tax, array( $post ), $args );
	flush_rewrite_rules();

}

