<?php 
//---------------------------------------------------------------------*/
//  Slider ACTION
//---------------------------------------------------------------------*/

add_action('init','add_multithumbnail');


//---------------------------------------------------------------------*/
//  Slider THEME SUPPORT
//---------------------------------------------------------------------*/
add_action( 'admin_enqueue_scripts', 'avispaweb_slider_admin_styles' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-thumbnails', 'slider' );
add_action( "wp_enqueue_scripts", "avispaweb_slider_styles" );

//---------------------------------------------------------------------*/
//  Slider CUSTOM 
//---------------------------------------------------------------------*/
add_action( 'add_meta_boxes', 'add_meta_boxes_slider_url' );
add_action( 'save_post', 'save_slider_url' );
add_action( 'publish_post', 'save_slider_url');

//---------------------------------------------------------------------*/
//  Slider FUNCTIONS
//---------------------------------------------------------------------*/


	
// let's start by enqueuing our styles correctly
function avispaweb_slider_admin_styles() {
    wp_register_style( 'avispaweb_slider_admin_stylesheet', plugins_url( '../css/style-admin.css', __FILE__ ) );
    wp_enqueue_style( 'avispaweb_slider_admin_stylesheet' );
	wp_register_style( "slider-home",AW_DIR.'/css/slider-home.css',  __FILE__  );
	wp_enqueue_style( "slider-home");

}
function avispaweb_slider_styles() {
  	wp_register_style( "slider-home",AW_DIR.'/css/slider-home.css',  __FILE__  );
	wp_enqueue_style( "slider-home");

}


// add thumbnail
function add_multithumbnail(){
	if (class_exists('MultiPostThumbnails')) { 
		new MultiPostThumbnails(array(
		'label' => 'Fondo Slider',
		'id' => 'background-slider',
		'post_type' => 'slider'
		 ) );
	}

}


function add_meta_boxes_slider_url() {
	add_meta_box("slider_url", __("Configuración Item Slider", 'textdomain' ), "add_slider_url", __("Slider", 'textdomain' ), "normal", "high");
}

function add_slider_url() {
global $post;
$custom = get_post_custom( $post->ID );
	
if (class_exists('MultiPostThumbnails')) { 
	$imageid = MultiPostThumbnails::get_post_thumbnail_id(get_post_type(), 'background-slider', $post->ID);	
 } else {		
	echo '<div class="error"><p><strong>'.__('Atención: Para usar correctamente este Plugin, necesitas instalar y activar  el complemento de miniaturas','textdomain').' <a href="http://www.totalnet.cl/wp-admin/plugin-install.php?tab=plugin-information&amp;plugin=multiple-post-thumbnails&amp;TB_iframe=true&amp;width=772&amp;height=853" class="thickbox" aria-label="Más información sobre Multiple Post Thumbnails 1.6.6" data-title="Multiple Post Thumbnails 1.6.6">Multiple Post Thumbnails</a></strong></p></div>';
	// Attention: In order to correctly use this Plugin, you need to install and activate the thumbnail plugin
	}
	
$imageurl = wp_get_attachment_image_src($imageid,'large');
?>

<div class="tabset">

  <!-- Tab 1 -->
  <input type="radio" name="tabset" id="tab1" aria-controls="inicio" checked>
  <label for="tab1"><?php echo __("Slider Easy AvispaWeb","textdomain");?></label>
  <!-- Tab 2 -->
  <input type="radio" name="tabset" id="tab2" aria-controls="imagen">
  <label for="tab2"><?php echo __("Agregar Imagen Slider","textdomain");?></label>
  <!-- Tab 3 -->
  <input type="radio" name="tabset" id="tab3" aria-controls="video">
  <label for="tab3"><?php echo __("Agregar Video Slider","textdomain");?></label>
  
  <div class="tab-panels">
    <section id="inicio" class="tab-panel">
   <h2 style="font-size: 2em;"><?php echo __('Slider Video Full', 'textdomain' );?></h2>
	<p>Usar Video Full o Imagen + Texto y Enlace</p>
	<p>Elegir Tipo de Slider a Crear</p>
	Si deseas agregar un video FullScreen, debes ingresar la URL del video y su imagen en el TAB->Agregar Video Slider
  </section>
	  <section id="imagen" class="tab-panel">
	  <?php require_once("_slider-imagen.php");?>

	  </section>
	   <section id="video" class="tab-panel">
	  		<?php require_once("_slider-video.php");?>
	  </section>
  </div>  
</div>

<?php }

function save_slider_url(){

  global $post;
global $post_type;
  if ( isset($post->ID) && $post_type=== 'slider') {	
/* 

Titulo  the_title()
Contenido o texto the_content()
Links interno o externo @$custom["slider_url"][0]
Links target @$custom["slider_url_target"][0]
Boton link  @$custom["slider_btn"][0]


Video 
Titulo video @$custom["slider_video_title"][0]
Link a Video @$custom["slider_url"][0]

 
Imagen has_post_thumbnail()
slider_fondo_pattern
slider_fondo_color
*/
	
if (isset($_POST["slider_url"]))		update_post_meta($post->ID, 'slider_url', @$_POST["slider_url"], false); 
if (isset($_POST["slider_btn"]))		update_post_meta($post->ID, 'slider_btn', @$_POST["slider_btn"], false); 
if (isset($_POST["slider_url_target"]))		update_post_meta($post->ID, 'slider_url_target', @$_POST["slider_url_target"], false); 
if (isset($_POST["slider_video_img"]))		update_post_meta($post->ID, 'slider_video_img', @$_POST["slider_video_img"], false); 
if (isset($_POST["slider_video_title"]))		update_post_meta($post->ID, 'slider_video_title', @$_POST["slider_video_title"], false); 
if (isset($_POST["slider_video_url"]))		update_post_meta($post->ID, 'slider_video_url', @$_POST["slider_video_url"], false); 
if (isset($_POST["slider_fondo_pattern"]))		update_post_meta($post->ID, 'slider_fondo_pattern', @$_POST["slider_fondo_pattern"], false); 
if (isset($_POST["slider_fondo_color"]))		update_post_meta($post->ID, 'slider_fondo_color', @$_POST["slider_fondo_color"], false); 

  }

}
function loop_slider_avispa($category){	
		include_once("_slider-loop.php");
}


add_filter( 'manage_posts_columns', 'slider_columns_header' );
add_action( 'manage_posts_custom_column' , 'custom_column_content', 10, 2 );

function slider_columns_header($columns) {

 $columns['featured_image'] = 'Featured Image';
    return $columns;
}

function custom_column_content($column_name, $post_ID) {
	global $post_type;

    switch ( $column_name ) {


       

		case 'featured_image':
			  if ( $post_type=== 'slider') {	
	  
$imageid = MultiPostThumbnails::get_post_thumbnail_id(get_post_type(), 'background-slider',$post_ID);	
	  $image = wp_get_attachment_image_src($imageid,'thumbnail');
	  $imageurl = $image[0];
  }else{
			$image_attributes = wp_get_attachment_image_src ( get_post_thumbnail_id ( $post_ID), array(50,50) ) ; 
			$imageurl= $image_attributes[0];
		}	
		
            echo '<img src="' . $imageurl . '" />';
		 break;
    }
}

?>