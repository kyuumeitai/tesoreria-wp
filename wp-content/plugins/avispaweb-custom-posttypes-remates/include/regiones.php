<?php


// Register and load the widget
function regiones_load_widget() {
    register_widget( 'regiones_chile_widget' );
//
}
add_action( 'widgets_init', 'regiones_load_widget' );
//add_shortcode('regiones_chile_widget', 'regiones_chile_widget');add_filter('regiones_chile_widget', 'do_shortcode');

// Creating the widget 
class regiones_chile_widget extends WP_Widget {
 
function __construct() {

parent::__construct(
 
// Base ID of your widget
'regiones_chile_widget', 
 
// Widget name will appear in UI
__('Regiones de Chile', 'regiones_chile_widget_domain'), 
 
// Widget description
array( 'description' => __( 'Regiones de Chile ', 'regiones_chile_widget_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
 
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
// This is where you run the code and display the output
//echo __( 'Hello, World!', 'regiones_chile_widget_domain' );
echo $contenido=search_regiones();

echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance['title'] ) ) {
$title = $instance['title'];
}
else {
$title = __( 'Title', 'regiones_chile_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}


} // Class regiones_chile_widget ends here

function search_regiones(){
	$feed_url = dirname(__FILE__).'\/chile-regiones.xml';
    $feed = simplexml_load_file($feed_url) ? simplexml_load_file($feed_url) : null;
    // $Token = json_decode($respuesta, true);
	$content = json_decode(json_encode($feed),TRUE);
    $items = $content ?  $content['region'] : array();
    $count = count($items);
   // echo "<pre>". $count ;  

   foreach ($items as $key => $value) { 	
   	  foreach ($value['provincia'] as $k => $v) {
   	  		//print_r($v);
   	  	  	 foreach ($v['comuna'] as $c => $comuna) {
   	  	  	 	$title =$comuna['@attributes']['nombre'];

				//$title = str_replace(array('á','é','í','ó','ú','ñ'),array('a','e','i','o','u','n'),$title);
				$title = sanitize_title($title);
   	  	  	 	$t[$title]=  $comuna['@attributes']['nombre'];
   	  	  	 	//$t[]=  utf8_decode( strtoupper($comuna['@attributes']['nombre']));
   	  	  	$chile[$key]=['region'=>$value['@attributes']['nombre'],
   	  	  		'comuna' =>$t ];
   	  	// 	$region[$value['@attributes']['nombre']]['comuna'][]= $comuna['@attributes']['nombre'];

   	  	 	# code...
   	  	 }
   	  	# code...
   	  }
			$t=array();
   	# code...
   } 


    	$term_list ="";
    	 get_term_by('name', 'vigente' , 'vigente');
		$args = array( 'hide_empty=0' ); 
$terms = get_terms( array( 
    'taxonomy' => 'comuna',
    'parent'   => 0,'hide_empty' =>0
) );
/* echo "<pre>"; print_r($terms);
echo "</pre>"; */
if (  $terms  ) {
        $term_list = '<form id="search_region" name="search_region" action="'.get_bloginfo( 'url' ).'/busqueda/" method="POST">';
    $term_list .= '<select name="region" id="region" class="form-control">
						<option value="">Seleccionar region</option>';
   foreach ( $chile as $k => $c ) {   
		$term_list .= '<option value="'.$c['region'] .'" data-comunas="';     
$com=array(); 

foreach ($c['comuna'] as $key => $v) {

foreach ($terms as $value) {	
        	# code...
        //	echo  strtolower($value->name); 
        	//print_r($c['comuna']);
       	if(  strtolower($value->slug) == $key){
       		$com[]=$value->name;
          $count=$value->count;
        	}
        }
	# code...
}
        
       	$term_list .=  implode(',', $com);
        $term_list .= '" data-total="'.$count.'">'.$c['region'] .'</option>';
    }
$term_list .= '</select>';
 $term_list .= '<select name="comuna" id="comuna" class="form-control"><option value="">Seleccionar comuna</option>';
$term_list .= '</select>';
    $term_list .= '<input type="submit" id="enviar" name="enviar" value="Buscar"  class="form-control"/>';
    $term_list .= '</form>';

	echo $term_list;
	wp_reset_query();
     return;
}


}
add_action( "wp_enqueue_scripts", "archivo_scripts" );
function archivo_scripts() {
wp_register_script( "regiones",  AW_DIR."/js/scriptpost.js",  array("jquery"),"1.0"  );
wp_enqueue_script( "regiones" );
}
?>