<?php 

function add_meta_boxes_archivo() {

	add_meta_box( "add_archivo_adjunto", "Documento Adjunto", "add_archivo_adjunto", "servicio", "side", "high" );
}
function add_archivo_adjunto() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$tipo_post = get_post_type( $post->ID );
	$datos = new load_posttypes();
	?>
	<div class="archivo_archivo">
		<?php if ( isset($custom["archivo-adjunto"][0])): 	
$archivo=$custom["archivo-adjunto"][0];
$datos->show_descarga($archivo);

	?>



		<?php endif; ?>
		<div class="file-upload">
			<label for="file" class="file-upload__label button-primary">ADJUNTAR <?php if ( isset($custom["file_archivo"][0] )): ?>NUEVO <?php endif; ?>ARCHIVO</label>
			<input id="file" class="file-upload__input" type="file" name="file">
		</div><br/>
		<small>Subida máx. de archivo: <?php echo  ini_get('upload_max_filesize');?></small>
		<div id="respuesta" class="alert"></div>
		<progress id="barra_de_progreso" value="0" max="100"></progress>
		<div id="message_file"></div>
	</div>
		<input type="hidden" id="archivo-adjunto" name="archivo-adjunto" value="<?php echo (isset($custom["archivo-adjunto"][0]))? @$custom["archivo-adjunto"][0]:" ";?>">

	
		<?php // <h3>Archivos subidos anteriormente</h3>
	//$datos->lista_archivos_adjuntos($post->ID);
	?>

	<?php 

}
function save_archivo() {
	global $post;
	global $post_type;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post->ID;
	}
	  if ( isset($post->ID) && $post_type=== 'servicio') {	
		if ( isset($_POST[ "archivo-adjunto" ] ) ) {
		update_post_meta( $post->ID, "archivo-adjunto", @$_POST[ "archivo-adjunto" ] );
		
		}
	  } 
	
}


// add thumbnail
function add_multithumbnail_servicio(){
	if (class_exists('MultiPostThumbnails')) { 
		new MultiPostThumbnails(array(
		'label' => 'Fondo Slider',
		'id' => 'background-slider',
		'post_type' => 'slider'
		 ) );
	}

}
/*
add_action( 'admin_init', 'add_meta_boxes_archivo' );
add_action( 'save_post', 'save_archivo' );
add_action( 'publish_post', 'save_archivo' );
*/


add_action( 'add_meta_boxes', 'add_meta_boxes_planes_servicios' );
add_action( 'save_post', 'save_planes_servicios' );
add_action('publish_post', 'save_planes_servicios');
function add_meta_boxes_planes_servicios() {
add_meta_box("datos_planes_servicios", "Información del Plan", "add_planes_servicios","planes_servicios", "normal", "high");
}

function add_planes_servicios() {
	global $post;
	$custom = get_post_custom( $post->ID );
?><h3>Descripción del Plan</h3>
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="table-style">
	<tr>
<td width="29%" align="left"><h2>Valor Plan</h2></td>
<td width="71%" align="left"><input type="text" name="plan_valor" id="plan_valor" value="<?php echo @$custom["plan_valor"][0]; ?>"/>	
	</td></tr>
	
<?php
	 $titulo=array("Servidor","GB disponibles","Cuentas de Correo","Dominios Adicionales" ,"Dominios Apuntados","Subdominios","Cuentas FTP", "Bases de datos Mysql");
	
		for($i=0;$i<count($titulo);$i++) { ?>
<tr>
<td align="left"><h4><?php echo $titulo[$i];?></h4></td>
<td align="left"><input type="text" name="texto_plan_<?php echo $i;?>" id="texto_plan_<?php echo $i;?>" value="<?php echo @$custom["texto_plan_".$i][0]; ?>"/>	
	</td></tr> 
	<?php }


?>
	<tr>
		<td>Otras Caracteristicas</td>
		<td><?php
$field_value = get_post_meta( $post->ID, 'plan_general', false );
  wp_editor( $field_value[0], 'plan_general' ); ?></td></tr>
	</table>

<?php 

}

function save_planes_servicios(){

  global $post;
	global $post_type;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post->ID;
	}

   if ( isset($post->ID) && $post_type=== 'planes_servicios') {	
	 $titulo=array("Servidor","GB disponibles","Cuentas de Correo","Dominios Adicionales" ,"Dominios Apuntados","Subdominios","Cuentas FTP", "Bases de datos Mysql");

	 $post_id = $post->ID;	 
	if (@$_POST["plan_general"]!=="" ){ update_post_meta($post_id, 'plan_general', @$_POST["plan_general"], false);} 
	if (@$_POST["plan_valor"]!=="" ){ update_post_meta($post_id, 'plan_valor', @$_POST["plan_valor"], false); }
	  	for($i=0;$i<count($titulo);$i++) {
	if (@$_POST["texto_plan_".$i]!=="" ){ update_post_meta($post_id, 'texto_plan_'.$i, @$_POST["texto_plan_".$i], false); } 
		}
  }

}


?>