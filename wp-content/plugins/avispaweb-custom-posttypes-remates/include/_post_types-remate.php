<?php 



function add_meta_boxes_archivo() {

	add_meta_box( "add_archivo_adjunto", "Documento Adjunto", "add_archivo_adjunto", "remate", "side", "high" );
}
function add_archivo_adjunto() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$tipo_post = get_post_type( $post->ID );
	$datos = new load_posttypes();
	?>
	<div class="archivo_archivo">
		<?php if ( isset($custom["archivo-adjunto"][0])): 	
$archivo=$custom["archivo-adjunto"][0];
$datos->show_descarga($archivo);

	?>



		<?php endif; ?>
		<div class="file-upload">
			<label for="file" class="file-upload__label button-primary">ADJUNTAR <?php if ( isset($custom["file_archivo"][0] )): ?>NUEVO <?php endif; ?>ARCHIVO</label>
			<input id="file" class="file-upload__input" type="file" name="file">
		</div><br/>
		<small>Subida máx. de archivo: <?php echo  ini_get('upload_max_filesize');?></small>
		<div id="respuesta" class="alert"></div>
		<progress id="barra_de_progreso" value="0" max="100"></progress>
		<div id="message_file"></div>
	</div>
		<input type="hidden" id="archivo-adjunto" name="archivo-adjunto" value="<?php echo (isset($custom["archivo-adjunto"][0]))? @$custom["archivo-adjunto"][0]:" ";?>">

	
		<?php // <h3>Archivos subidos anteriormente</h3>
	//$datos->lista_archivos_adjuntos($post->ID);
	?>

	<?php 

}
function save_archivo() {
	global $post;
	global $post_type;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post->ID;
	}
	  if ( isset($post->ID) && $post_type=== 'remate') {	
		if ( isset($_POST[ "archivo-adjunto" ] ) ) {
		update_post_meta( $post->ID, "archivo-adjunto", @$_POST[ "archivo-adjunto" ] );
		
		}
	  } 
	
}



/*
add_action( 'admin_init', 'add_meta_boxes_archivo' );
add_action( 'save_post', 'save_archivo' );
add_action( 'publish_post', 'save_archivo' );
*/


add_action( 'add_meta_boxes', 'add_meta_boxes_remate' );
add_action( 'save_post', 'save_remate' );
add_action('publish_post', 'save_remate');
function add_meta_boxes_remate() {
add_meta_box("datos_remate", "Información del Remate", "add_remate","remate", "normal", "high");
}

function add_remate() {
	global $post;
	$custom = get_post_custom( $post->ID );

global $rematesVariables;

?>
<table  border="0" cellpadding="5" cellspacing="0" class="table">

<?php

	foreach ($custom as $key => $value) {
		if( '_edit_lock'!==$key){
		 ?>
		
<tr>
<td align="left"><h4><?php echo $key;?></h4></td>
<td align="left"><input type="text"  value="<?php echo $value[0]; ?>" readOnly/>	
	</td></tr> 
	<?php 
}
	}
		


?>
	
	</table>

<?php 

}

function save_remate(){

  global $post;
	global $post_type;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post->ID;
	}

   if ( isset($post->ID) && $post_type=== 'remate') {	
	 $titulo=array("Servidor","GB disponibles","Cuentas de Correo","Dominios Adicionales" ,"Dominios Apuntados","Subdominios","Cuentas FTP", "Bases de datos Mysql");

	 $post_id = $post->ID;	 
	if (@$_POST["Remate_general"]!=="" ){ update_post_meta($post_id, 'Remate_general', @$_POST["Remate_general"], false);} 
	if (@$_POST["Remate_valor"]!=="" ){ update_post_meta($post_id, 'Remate_valor', @$_POST["Remate_valor"], false); }
	  	for($i=0;$i<count($titulo);$i++) {
	if (@$_POST["texto_Remate_".$i]!=="" ){ update_post_meta($post_id, 'texto_Remate_'.$i, @$_POST["texto_Remate_".$i], false); } 
		}
  }

}


?>