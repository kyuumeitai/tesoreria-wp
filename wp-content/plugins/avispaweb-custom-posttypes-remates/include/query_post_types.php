<?php // CONSULTA
// https://andres-dev.com/utilizando-la-clase-wpdb-de-wordpress/
///https://www.ecodeup.com/variables-constantes-clases-y-objetos-en-php/


class load_posttypes{
	public $datos;
	public $tipo;
	public $nametipo;


	public function show_descarga($id){
		//var_dump($id);
		if (!isset($id) || $id==" "){
			return;
		} 
			
		global $wpdb;
		$table_name= $wpdb->prefix . "posts";	
		$adjunto=$wpdb->get_row("SELECT * FROM $table_name p WHERE p.post_type = 'archivo-adjunto' AND  p.ID = ". $id);
		$ext=$this->extension($adjunto->guid);
		//$bytes = ($adjunto->guid)? $this->human_filesize($this->getSizeFile($adjunto->guid)) : 0;
		$bytes = ($adjunto->guid)? $this->human_filesize(@filesize($adjunto->guid)) : 0;
		$wpdb->flush(); 
		?>
		<div class="descarga center-block">
		<table class="table table-bordered">
			<tbody>
				<tr><td rowspan="2" style="width: 50px;"><i class="fa fa-file fa-file-<?php echo $ext;?>-o pull-left" aria-hidden="true"></i></td><td><?php echo $adjunto->post_title;?></td></tr>
				<tr><td>Peso:<?php echo  $bytes; ?>  </td></tr>
				<tr><td colspan="2"><button  type="button" data-id="<?php echo (isset($adjunto->ID))? $adjunto->ID:" ";?>" class="file_archivo_path btn btn-success"><i class="fa fa-download pull-left" aria-hidden="true"></i> Descargar</button>
</td></tr>	</tbody></table>
</div>
<?php 
		
	}	
	public function show_adjunto($id){
		//var_dump($id);
		if (!isset($id) || $id==" "){
			return;
		} 
			
		global $wpdb;
		$table_name= $wpdb->prefix . "posts";
		$adj=array();
		$adj=$wpdb->get_row("SELECT * FROM $table_name p WHERE p.post_type = 'archivo-adjunto' AND  p.ID = ". $id);
		$ext=$this->extension($adj->guid);
		$bytes = ($adj->guid)? $this->human_filesize(@filesize($adj->guid)) : 0;
		$wpdb->flush(); 
		$datos=array("ext"=>$ext,"bytes"=>$bytes);
		return $datos;
		?>

<?php 
		
	}
	public function lista_archivos_adjuntos($id){
	
	$sql= array( 'post_type' => 'archivo-adjunto','order' => 'ASC','orderby'=>'date','post_status' => 'publish','post_parent'=>$id);
		query_posts($sql);
		if (have_posts()) :	while (have_posts()) : the_post();
			global $post;	
		 echo '<div class="checkbox"><label for="archivo_adjunto_'.$post->ID.'">
			<input type="checkbox" name="archivo_adjunto[]" id="archivo_adjunto_'.$post->ID.'" value="'.$post->ID.'" '.$check.'>'.$post->post_title.'</label></div>';
	
	endwhile; endif; wp_reset_query();
	}
	public function lista_tax($term){
		$term_list ="";
		$args = array( 'hide_empty=0' ); 
		$terms = get_terms( $term, $args );
if (  $terms  ) {
    
    $i = 0;
    $term_list = '<ul class="list-inline">';
    foreach ( $terms as $term ) {
        $i++;
	
        $term_list .= '<li><a href="' . esc_url( get_term_link( $term ) ) . '" title="Ir a Categoría de ' . $term->name . '">' . $term->name . '</a></li>';
       
    }$term_list .= '</ul>';
	
	wp_reset_query();
   
} return $term_list;
	}	
	public function lista_post_tax($id,$term,$clases,$sep){
		$args = array( 'hide_empty=0' ); 
		$terms = get_the_terms( $id,$term);
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
    $count = count( $terms );
    $i = 0;
			$term_list="";
   $esp=$sep;
    foreach ( $terms as $key => $term ) {
    	if($key == count($terms)-1) { $esp=" "; }
		$term_list .= ' <span  ><a  href="' . esc_url( get_term_link( $term ) ) . '" title="Ir a Categoría de ' .$term->name . '" class="'.$clases.'">' . $term->name .$esp. '</a></span>';
       }
			
    return $term_list;
	wp_reset_query();
}
	}
	
	public function extension($archivo){
	 $extension = pathinfo($archivo, PATHINFO_EXTENSION);
   $extension = strtolower($extension);
	// echo $extension;
	 switch  ($extension){
		 case "pdf":
			$ext="pdf";
			 break;
		 case "xls":
		 case "xlsx":
			$ext="excel";
			 break;
			 
		 case "doc":
		 case "docx":
			$ext="word";
			 break;
		 case "gif":
		 case "jpg":
		 case "jpeg":
		 case "png":
			$ext="image";
			 break;
		 case "pptx":
			$ext="powerpoint";
			 break;
		 default:
			 $ext="file";
	 }
	return $ext;
}
	public function megas($url) {
		if(file_exists($url))
			return @filesize($url);
		
	}
	public function getSizeFile($url) {
    if (substr($url,0,4)=='http') {
        $x = array_change_key_case(get_headers($url, 1),CASE_LOWER);
        if ( strcasecmp($x[0], 'HTTP/1.1 200 OK') != 0 ) { $x = $x['content-length'][1]; }
        else { $x = $x['content-length']; }
    }
    else { $x = @filesize($url); }

    return $x;
} 

	public function human_filesize($bytes, $decimals = 2) {
    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor > 0) $sz = 'KMGT';
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
}
	/* FUNCIONES PHP */

public function url_exists( $url = NULL ){
 if( empty( $url ) ){
        return false;
    }

    $ch = curl_init( );
 $options = array(
 CURLOPT_URL            =>  $url,
 CURLOPT_RETURNTRANSFER => true,
 CURLOPT_HEADER         => true,
 CURLOPT_FOLLOWLOCATION => true,
 CURLOPT_ENCODING       => "",
 CURLOPT_AUTOREFERER    => true,
 CURLOPT_CONNECTTIMEOUT => 120,
 CURLOPT_TIMEOUT        => 120,
 CURLOPT_MAXREDIRS      => 10,
 );
 curl_setopt_array( $ch, $options );
 $response = curl_exec($ch);
 $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
 
if ( $httpCode != 200 ){
	//echo $httpCode;
$respuesta =" Return code is {$httpCode} \n" 
	.curl_error($ch);
 } else {
	$respuesta=$httpCode;
//	echo  $url
 //echo "<pre>".htmlspecialchars($response)."</pre>";
 }
	curl_close($ch);
 return $respuesta;


	
}

}


?>