<?php


// Register and load the widget
function tribunales_chile_load_widget() {
    register_widget( 'tribunales_chilewidget' );
}
add_action( 'widgets_init', 'tribunales_chile_load_widget' );
 
// Creating the widget 
class tribunales_chilewidget extends WP_Widget {
 
function __construct() {
parent::__construct(
 
// Base ID of your widget
'tribunales_chilewidget', 
 
// Widget name will appear in UI
__('Tribunales de Chile', 'tribunales_chilewidget_domain'), 
 
// Widget description
array( 'description' => __( 'Tribunales de Chile ', 'tribunales_chilewidget_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
 
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
// This is where you run the code and display the output
//echo __( 'Hello, World!', 'tribunales_chilewidget_domain' );
echo $contenido=search_tribunales();

echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance['title'] ) ) {
$title = $instance['title'];
}
else {
$title = __( 'Title', 'tribunales_chilewidget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}


} // Class tribunales_chilewidget ends here

function search_tribunales(){

$terms = get_terms( array( 
    'taxonomy' => 'comuna',
    'parent'   => 0,'hide_empty' =>true
) );
/*echo "<pre>"; print_r($terms);
echo "</pre>";*/
if (  $terms  ) {
    $term_list = '<form id="search_tribunal" name="search_tribunal" action="'.get_bloginfo( 'url' ).'/busqueda/" method="POST">';
    $term_list .= '<select name="comuna_tribunal" id="comuna_tribunal" class="form-control">
						<option value="">Seleccionar Comuna</option>';
  

foreach ($terms as $value) {	
$term_list .= '<option value="'.$value->term_id .'" data-tribunal="';     

  $term_tribunales =get_terms( array( 
    'taxonomy' => 'tribunal','include' => 'all',
    'child_of'   => $value->term_id,'hide_empty' =>true
) );
/*
echo "<pre>"; print_r($term_tribunales);
echo "</pre>";*/

foreach ($term_tribunales as $k => $trib) {

$com[]=$trib->name;
  # code...
}
  $term_list .=  implode(',', $com);
  $term_list .= '">'.$value->name .'</option>';
$com=array(); 


        }
	# code...

        
      
    
$term_list .= '</select>';
$term_list .= '<select name="tribunal" id="tribunal" class="form-control">';
$term_list .= '</select>';
    $term_list .= '<input type="submit" id="enviar" name="enviar" value="Buscar"  class="form-control"/>';
    $term_list .= '</form>';

	echo $term_list;
	wp_reset_query();
     return;
}


}


?>