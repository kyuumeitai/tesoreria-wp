<h3>
	<?php echo __('Agregar Slider tipo Video Full Pantalla (Opcional)', 'textdomain' );?>
</h3>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="table-style">
		<tr>
		<td align="left">
			<?php echo __('Título de video', 'textdomain' );?>
		</td>
		<td width="87%" align="left"><input type="text" name="slider_video_title" id="slider_video_title" value="<?php echo @$custom["slider_video_title"][0]; ?>" size="30" placeholder="Título"/>
		</td>
	</tr>
	<tr>
		<td width="13%" align="left">
			<?php echo __('Enlace de imagen para carga de video <small> <a href="/wp-admin/media-upload.php?post_id='.$post->ID.'&amp;type=image&amp;TB_iframe=1&referer=slider_video_img" class="thickbox" aria-label="'. __('Subir Imagen', 'textdomain' ).'" data-title="'.__('Subir Imagen', 'textdomain' ).'">'. __('Subir Imagen', 'textdomain' ).'</a></small>', 'textdomain' );?>
		</td>
		<td width="87%" align="left"><input type="text" name="slider_video_img" id="slider_video_img" value="<?php echo @$custom["slider_video_img"][0]; ?>" size="30" placeholder="http://"/>
		</td>
	</tr>
	<tr>
		<td align="left">
			<?php echo __('Url de video <small> <a href="/wp-admin/media-upload.php?post_id='.$post->ID.'&amp;type=video&amp;TB_iframe=1&referer=slider_video_url" class="thickbox" aria-label="'. __('Subir Video', 'textdomain' ).'" data-title="'.__('Subir Video', 'textdomain' ).'">'. __('Subir Video', 'textdomain' ).'</a></small>', 'textdomain' );?>
		</td>
		<td width="87%" align="left"><input type="text" name="slider_video_url" id="slider_video_url" value="<?php echo @$custom["slider_video_url"][0]; ?>" size="30" placeholder="http://"/>
		</td>
	</tr>
</table>