<?php 
/* 
Function para crear PostTypes y taxonimies

dashicons: https://developer.wordpress.org/resource/dashicons/#format-image

POST_TYPE. público, con slug personalizado y permisos excluivos. ejempl: noticias, servicios

$args=array('public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'menu_position' => 3,
		'supports' => $supports,
		'menu_icon' => 'dashicons-camera',);
		
POST_TYPE. público, sin URL. ejempl: Slider
		
$args_nopublic=array('public' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array( 'with_front' => false ),
        'query_var' => true,
        'supports' => $supports,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-camera');
*/
if ( ! class_exists('AW_Create_Post_Types') ) {

class AW_Create_Post_Types{
private $post_type;
private $post_slug;
private $custom_plural;
private $args;

public function __construct( $post_type = null, $custom_plural = false, $args_post = array() ) {
	if ( !$post_type ) {

		return;

	}

	$this->post_type = ucwords($post_type);
	$this->post_type_plural = ( $custom_plural ) ? ucwords($custom_plural) : ucwords($post_type) . 's';
	$this->args=$args_post;
	$this->supports =  ($args_post['supports']);
	
	$this->post_slug = $post_type;
	$this->add_actions();
}
public function create_capabilities( $post ) {
	$posts = $post . 's';
	$capabilities = array(
		'create_posts' => 'create_' . $post,
		'delete_others_posts' => 'delete_others_' . $posts,
		//'delete_post' => 'delete_'.$post,
		'delete_posts' => 'delete_' . $posts,
		'delete_private_posts' => 'delete_private_' . $posts,
		'delete_published_posts' => 'delete_published_' . $posts,
		'edit_others_posts' => 'edit_others_' . $posts,
		'edit_posts' => 'edit_' . $posts,
		'edit_private_posts' => 'edit_private_' . $posts,
		'edit_published_posts' => 'edit_published_' . $posts,
		'manage_categories' => 'manage_terms',
		'publish_posts' => 'publish_' . $posts,
		'read_private_posts' => 'read_private_' . $posts,

	);
	return $capabilities;
}
public function create_post_types() {
	/*	echo "<pre>";	print_r($this->args) ;

	echo "</pre>";*/
	$args = array(
		'labels' => array(
			'name' => $this->post_type_plural,
			'singular_name' => $this->post_type,
			'add_new' => 'Agregar Nuevo ' . $this->post_type,
			'add_new_item' => 'Agregar ' . $this->post_type,
			'edit_item' => 'Editar' . $this->post_type,
			'new_item' => 'Categorías ' . $this->post_type,
			'view_item' => 'Ver ' . $this->post_type,
			'search_items' => 'Buscar ' . $this->post_slug,
			'not_found' => 'No encontrada ',
			'not_found_in_trash' => 'No encontrada en papelera',
			'parent_item_colon' => '',
		),
		'public' => $this->args['public'],
		'publicly_queryable' => $this->args['publicly_queryable'],
		'show_ui' => $this->args['show_ui'],
		'show_in_menu' => $this->args['show_in_menu'],
		'show_in_rest' => $this->args['show_in_rest'],
		'query_var' => $this->args['query_var'],
		'rewrite' => array( 'slug' => $this->post_slug),
		'capabilities' =>array($this->post_slug,'create_capabilities' ),
		//'capability_type' => array($this->post_type,$this->post_slug),
		'map_meta_cap' => true,
		'has_archive' => $this->args['has_archive'],
		'hierarchical' => $this->args['hierarchical'],
		'exclude_from_search' => $this->args['exclude_from_search'],
		'supports' => $this->supports,
		'menu_position' => $this->args['menu_position'],
		'menu_icon' => $this->args['menu_icon'],

	);
 //do_action( 'registered_post_type', $this->post_type, $args  );
	register_post_type( $this->post_slug, $args );
	//	print_r($args);
	flush_rewrite_rules();
}

public function add_actions() {
	add_action( 'init', array( $this, 'create_post_types' ) );
}
//Categoria Servicios
public function category_posttype_init() {

	$labels = array(
		'name' => __( 'Categoría de ' . $this->post_slug, 'taxonomy general name', 'textdomain' ),
		'singular_name' => __( 'Categoría de ' . $this->post_type, 'taxonomy singular name', 'textdomain' ),
		'search_items' => __( 'Search Categoría de ' . $this->post_type, 'textdomain' ),
		'all_items' => __( 'Todo Categoría de ' . $this->post_type, 'textdomain' ),
		'parent_item' => __( 'Parent Categoría de ' . $this->post_type, 'textdomain' ),
		'parent_item_colon' => __( 'Parent Categoría de ' . $this->post_type, 'textdomain' ),
		'edit_item' => __( 'Edit Categoría de ' . $this->post_type, 'textdomain' ),
		'update_item' => __( 'Update Categoría de ' . $this->post_type, 'textdomain' ),
		'add_new_item' => __( 'Agregar nueva Categoría de ' . $this->post_type, 'textdomain' ),
		'new_item_name' => __( 'Nuevo Nombre Categoría de ' . $this->post_type, 'textdomain' ),
		'menu_name' => __( 'Categoria de ' . $this->post_type, 'textdomain' ),
	);

	$args = array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => $this->post_type ),
	);

	register_taxonomy( 'type_' . $this->post_type, array( $this->post_type ) );

}}
}
?>