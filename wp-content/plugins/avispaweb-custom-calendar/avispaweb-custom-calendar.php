<?php
/*
Plugin Name: CALENTARIO REMATES
Plugin URI: http://www.avispaweb.com
Description: 
[CALENDAR_AW category=""] 
[CALENDAR_AW template=""] por default es horizontal con movimientos, vertical sin movemiento
[CALENDAR_AW cols=""] por default 1

**** version 1.9.. cantidad web items a mostrar
 ?>;
Author: Marcela Godoy
Version: 2.5
Author URI: http://www.mentalidadweb.com
*/
if ( !defined('CALENDAR_AW_PLUGIN_DIR') ) 	{
	defined('CALENDAR_AW_PLUGIN_DIR',  '/wp-content/plugins/'.plugin_basename( dirname(__FILE__)));
}
//error_reporting(E_ALL);
/* ini_set('error_reporting', E_ALL);

// error_reporting(E_ALL);
 ini_set('display_errors', '1'); */
define('CALENDAR_AW_VERSION', '1.0');



// Register and load the widget
function wpb_load_widget() {
    register_widget( 'calendar_aw' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
 
// Creating the widget 
class calendar_aw extends WP_Widget {
 
function __construct() {
parent::__construct(
 
// Base ID of your widget
'calendar_aw', 
 
// Widget name will appear in UI
__('Custom Widget', 'calendar_aw_domain'), 
 
// Widget description
array( 'description' => __( 'Your Custom Wordpress Widget ', 'calendar_aw_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
 
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
 
// This is where you run the code and display the output
echo __( 'Hello, World!', 'calendar_aw_domain' );
/* sample usages */
echo '<h2>July 2009</h2>';
echo draw_calendar(7,2009);

echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'calendar_aw_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}

/* draws a calendar */
private function draw_calendar($month,$year){

	/* draw table */
	$calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

	/* table headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	/* row for week one */
	$calendar.= '<tr class="calendar-row">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np"> </td>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			$calendar.= '<div class="day-number">'.$list_day.'</div>';

			/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
			$calendar.= str_repeat('<p> </p>',2);
			
		$calendar.= '</td>';
		if($running_day == 6):
			$calendar.= '</tr>';
			if(($day_counter+1) != $days_in_month):
				$calendar.= '<tr class="calendar-row">';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			$calendar.= '<td class="calendar-day-np"> </td>';
		endfor;
	endif;

	/* final row */
	$calendar.= '</tr>';

	/* end the table */
	$calendar.= '</table>';
	
	/* all done, return result */
	return $calendar;
	}
}
/*
class Plugin_CALENDAR_AW {
	var $opt;
	var $table_name;
function Plugin_CALENDAR_AW() { 

	$filepath    = 'admin.php?page='.$_GET['page'];
	global $wpdb; //que es el encargado de llevar la gesti�n de bases de datos para wordpress. Siempre que queramos interactuar con la base de datos de wordpress ser� necesaria esta llamada.
    register_activation_hook(__FILE__,  'CreateTable'); 
	//register_activation_hook(__FILE__,  'install'); //gancho para instalar
	register_deactivation_hook(__FILE__,'DropTable'); //gancho para desinstalar
// 	add_action('admin_menu', 'administrar_Carousel_menu');
	add_shortcode('CALENDAR_AW', 'CALENDAR_AW');
	add_filter('widget_text', 'do_shortcode');
//f	add_action('admin_init', 'script_maps_carousel');
	//add_action('admin_init', 'script_maps');

//	add_action("plugins_loaded", "widget_maps_Type_init"); 
// registrar el widget FooWidget
	//add_action('widgets_init', create_function('', 'return register_widget("Widget_Maps_MW");'));
	}






// echo do_shortcode('[CALENDAR_AW category="admision"]'); 

function CALENDAR_AW($atts, $content = null) {
	
	extract( shortcode_atts( array(
		'template' => '',
		'category' => '',
		'cols'=>1,
	), $atts ) );
	global $wpdb;
	$items   = $wpdb->prefix . "carousel";
	$id=$_POST['id'];
	$total_item=$wpdb->get_var("SELECT COUNT(id) FROM $items");

//	add_action( 'wp_print_scripts', 'script_carousel_horizontal' );
	add_action( 'wp_print_styles', 'add_style' );

	

}

}	




	
$mp = new Plugin_CALENDAR_AW();

*/
?>