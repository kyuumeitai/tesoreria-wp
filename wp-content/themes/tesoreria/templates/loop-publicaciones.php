<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 
    global $post;

  $data = get_post_custom( $post->ID,'data' );

  $value = get_post_custom( $post->ID,'idRemates' );
 $idRemates=json_decode($value['idRemates'][0]);

?>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
<div class="panel panel-default">
            <div class="panel-heading">
              <a href="<?php the_permalink(); ?>"  ><h2 class="panel-title">Publicación N° <span class="label label-primary"><?php the_title(); ?></span></h2></a>
            </div>
            <div class="panel-body">
             <table class="table">
       
            <tbody>
              <tr>
                 <th colspan="2" class="text-center"><a href="<?php the_permalink(); ?>"  >Fecha de Primera Publicación
                <h3 class="m-top-0"><span class="label label-default"> <?php the_time('d'); ?> <?php the_time('F'); ?>, <?php the_time('Y'); ?></span></h3></a></th>
              
              </tr>
              <?php if($data['marcaSegundaPublicación'][0]==true) { ?>
              <tr>
                 <th>Fecha de Segunda Publicación</th>
                <td> <?php the_time('d'); ?> <?php the_time('F'); ?>, <?php the_time('Y'); ?></td>
              
              </tr>
                <?php } ?>
            <tr><th>Comuna</th> <td><?php echo $data['comunaJuzgado'][0];?></td> </tr>
            <tr><th>N° de Remates</th> <td><?php echo count($idRemates);?></td> </tr>
              <tr>
              
                <td colspan="2">
                  <?php  // print_r($data); ?>
<div class="btn-group btn-group-justified" role="group" aria-label="...">
  <a href="#" class=" btn btn-default btn-sm" role="button">Ver PDF</a><a href="<?php the_permalink(); ?>" class="btn btn-default btn-sm" role="button">Ver Remates</a></div>
       
                 </td>
              
              </tr>
            </tbody>
          </table>
            </div>
          </div>

    </div>
<?php endwhile;?>