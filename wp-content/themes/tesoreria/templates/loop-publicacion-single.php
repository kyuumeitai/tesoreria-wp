<?php
if ( have_posts() )
	while ( have_posts() ): the_post();
    global $post; 
  $value = get_post_custom( $post->ID,'idRemates' );
 $idRemates=json_decode($value['idRemates'][0]);
?>
 
 <h1 class="bg-danger">Remates publicados  <?php the_time('d'); ?>/<?php the_time('m'); ?>/<?php the_time('Y'); ?></h1>
 <a href="#" class=" btn btn-default btn-sm" role="button">Ver PDF</a>
          	<?php 
            $sqltribunal = array(
               'hierarchical' => 1,
               'show_option_none' => '',
               'hide_empty' => 0,
               'taxonomy' => 'tribunal'
            );
            $tribunal = get_categories($sqltribunal);
            foreach ($tribunal as $t):
 
$args = array(
  'post_type' => 'remate', 'post__in' => $idRemates,'post_status'=>array('future','publish'),'posts_per_page'=> -1,
  'tax_query' => array(
  'relation' => 'AND',
    array(
      'taxonomy' => 'tribunal',
      'field'    => 'term_id',
      'terms'    => array($t->term_id),
      'operator' => 'IN',
    ),
  ),
);
$loop = new WP_Query( $args );

if($loop->post_count>0){
  
  include("loop-remates.php");
}

 wp_reset_postdata();
?> 
			<?php endforeach;   ?>
			<?php 	  wp_reset_query();?>
<?php endwhile; ?>