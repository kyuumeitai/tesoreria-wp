<div class="texto-home"><h3>Búsqueda por ROL</h3> <form role="search" method="POST" class="search-form" action="<?php echo home_url( '/busqueda/' ); ?>">
    <label>
        <span class="screen-reader-text"><?php echo _x( 'Número de ROL', 'label' ) ?></span>
        <input type="search" class="search-field"
            placeholder="<?php echo esc_attr_x( '100100..', 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" name="rol"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </label>
     <input type="submit" class="search-submit"
        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form></div>