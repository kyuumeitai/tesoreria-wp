<footer class="main-footer">


  <div class="copesa hidden-xs">
     <?php  echo makeCorpInfo();  ?>
  </div>

  <div class="copesa hidden-xs">
     <?php  echo makeAfooter();  ?>
  </div>

</footer>

<div class="modal fade" id="mainNav" tabindex="-1" role="dialog" aria-labelledby="mainNavLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a class="close" data-dismiss="modal" aria-label="Close" href=""><span class="ltpicto-close"></span></a>
        <h4 class="modal-title" id="mainNavLabel">Menu</h4>
      </div>
      <div class="modal-body">
        <nav>
        	<ul class="clearfix">
            <?php
            /*  $menuPrincipal = dyd_menus_extract("primary");

              foreach ($menuPrincipal as $items) {
           ?>
                <li><a href="<?php echo $items->url; ?>"><?php echo $items->title; ?></a></li>
           <?php
              }*/
            ?>
        	</ul>
        </nav>
        <div class="multisites">
          <ul class="clearfix">
            <li class="main-link">
              <a href="http://www.latercera.com/" target="_blank">
                <div class="fav-icon fav-icon-lt"></div>
              </a>
            </li>
            <li>
              <a href="http://culto.latercera.com/" target="_blank">
                <div class="fav-icon fi-culto">
                  <span class="ltpicto-fav-culto"></span>
                </div><span>Culto</span>
              </a>
            </li>
            <li>
              <a href="http://www.latercera.com/canal/tendencias/" target="_blank">
                <div class="fav-icon fi-lt">
                  <span class="ltpicto-fav-tendencias"></span>
                </div><span>Tendencias</span>
              </a>
            </li>
            <li>
              <a href="http://www.latercera.com/la-tercera-tv/" target="_blank">
                <div class="fav-icon fi-lt">
                  <span class="ltpicto-fav-tv"></span>
                </div><span>La Tercera TV</span>
              </a>
            </li>
            <li>
              <a href="http://www.latercera.com/canal/reportajes/" target="_blank">
                <div class="fav-icon fi-lt">
                  <span class="ltpicto-fav-reportajes"></span>
                </div><span>Reportajes</span>
              </a>
            </li>
            <li>
              <a href="http://mtonline.cl/" target="_blank">
                <div class="fav-icon fi-black">
                  <span class="ltpicto-fav-mtonline"></span>
                </div><span>MT Online</span>
              </a>
            </li>
            <li>
              <a href="http://finde.latercera.com/" target="_blank">
                <div class="fav-icon fi-finde">
                  <span class="ltpicto-fav-finde"></span>
                </div><span>Finde</span>
              </a>
            </li>
            <li>
              <a href="http://www.latercera.com/canal/el-deportivo/" target="_blank">
                <div class="fav-icon fi-lt">
                  <span class="ltpicto-fav-eldeportivo"></span>
                </div><span>el Deportivo</span>
              </a>
            </li>
            <li>
              <a href="http://masdecoracion.latercera.com/" target="_blank">
                <div class="fav-icon fi-grey">
                  <span class="ltpicto-fav-masdeco"></span>
                </div><span>Más Deco</span>
              </a>
            </li>
            <li>
              <a href="http://www.biut.cl/" target="_blank">
                <div class="fav-icon fi-biut">
                  <span class="ltpicto-fav-biut"></span>
                </div><span>Biut</span>
              </a>
            </li>
            <li>
              <a href="http://www.revistamujer.cl/" target="_blank">
                <div class="fav-icon fi-black">
                  <span class="ltpicto-fav-mujer"></span>
                </div><span>Mujer</span>
              </a>
            </li>
            <li>
              <a href="http://www.glamorama.cl/" target="_blank">
                <div class="fav-icon fi-glam">
                  <span class="ltpicto-fav-glamorama"></span>
                </div><span>Glamorama</span>
              </a>
            </li>
            <li>
              <a href="http://www.paula.cl/" target="_blank">
                <div class="fav-icon fi-white">
                  <span class="ltpicto-fav-paula"></span>
                </div><span>Paula</span>
              </a>
            </li>
            <li>
              <a href="http://www.quepasa.cl/" target="_blank">
                <div class="fav-icon fi-lt">
                  <span class="ltpicto-fav-quepasa"></span>
                </div><span>Qué Pasa</span>
              </a>
            </li>
            <li>
              <a href="http://www.latercera.com/canal/negocios/" target="_blank">
                <div class="fav-icon fi-neg">
                  <span class="ltpicto-fav-negocios"></span>
                </div><span>Negocios</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<?php wp_footer(); /* this is used by many Wordpress features and plugins to work proporly */
?>
</body>

</html>