<?php 

// Template Name: TEMPORAL

 ?><!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-Full8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Avispaweb 2015 | En construcción</title>
<link href='http://fonts.googleapis.com/css?family=Fredericka+the+Great|Rock+Salt' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://www.avispaweb.com/wp-content/themes/twentyfifteen/css/foundation.css" />
<link rel="stylesheet" href="http://www.avispaweb.com/wp-content/themes/twentyfifteen/css/avispa-web.css" />
<script src="http://www.avispaweb.com/wp-content/themes/twentyfifteen/js/vendor/modernizr.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-67005233-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body>
<header class="hero">
  <h1><span class="avispa">AVISPA</span><span class="aweb">WEB</span><br>
    <span>Preparando el Ataque!</span></h1>
</header>
<footer class="footer">
  <div class="row">
    <div class="large-12 columns">
      <p class="slogan">Diseño y Desarrollo Web + Hosting</p>
      <div class="row">
        <div class="small-12 large-6 large-centered columns">
          
  <form action="https://docs.google.com/forms/d/1ejrBt8fxt8zOOSX1FkG6b5iW9Rjyo6B5TNvmRxDA_s0/formResponse" method="post" id="commentForm" 
name="commentForm" class="ss-forms footer-form	" data-mail="http://www.avispaweb.com/wp-content/themes/twentyfifteen/mail.php" data-redirect="http://www.avispaweb.com/te-avispaste/">
          <div class="row">
    <div class="large-12 small-12 columns">
             
<input type="text"  title="Escribe tu nombre por favor" id="entry_1217272583" class="required" name="entry.1217272583" placeholder="Nombre (*)" >
</div></div>
<div class="row">
<div class="large-12 small-12 columns"><input type="text" title="Escribe tu Email, para contactarte" id="entry_1469996039" class="email required" name="entry.1469996039"  placeholder="Email (*)">
</div></div>
<div class="row">
<div class="large-12 small-12 columns">
<input name="entry.1810503221" type="text"  class="required number" id="entry_1810503221" value=""  placeholder="Telefono (*)" title="Por favor ingresa tu número telefónico, sólo digitos">
</div></div>
<div class="row">
<div class="large-12 small-12 columns">
<input name="entry.21188757" type="text" class="" id="entry_21188757" placeholder="Sitio Web" ></div></div>
<div class="row">
<div class="large-12 small-12 columns">

<select id="entry_1035404913"  name="entry.1035404913" title="Escribe la razón de tu contacto, eso nos permitirá atenderte más rápido" class="required">
<option value ="" selected="selected">Mi consulta es por (*)</option>
<option value ="Desarrollo/Diseño de Sitios">Desarrollo/Diseño de Sitios</option>
<option value ="Hosting">Hosting</option>
<option value ="Mantenimiento de sitios web">Mantenimiento de sitios web</option>
<option value ="Consulta General">Consulta General</option>
<option value ="Aún  no lo sé">Aún  no lo sé</option>
</select>

</div></div>
<div class="row">
<div class="large-12 small-12 columns">

<textarea name="entry.502174801" id="entry_502174801" class="required" placeholder="Expláyate :" title="Necesitamos  más detalles!!" ></textarea>
</div></div>
<div class="row">
<div class="large-12 small-12 columns">

<input type="submit" class="accion" value="Enviar" name="submit" id="submit">
<small>(*) Datos necesarios.</small>
</div></div>

</form>
        </div>
      </div>
      <p class="copywrite">2015 © avispaweb.com</p>
    </div>
  </div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script> <script src="http://www.avispaweb.com/wp-content/themes/twentyfifteen/js/forms.min.js"></script> <script type="text/javascript">$(document).ready(function() {	$(".ss-forms").validate();								$(".ss-forms").submit(function (e) {            //llamada a mentalidaforms.js        
    $(this).mforms();            //desactivar submit        
	    e.preventDefault();        });});</script>
</body>
</html>