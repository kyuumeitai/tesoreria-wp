<?php get_header(); ?>
<div class="container" >

	<div class="row ">
   
      <div class="col-xs-12 col-md-12 col-lg-12 ">    
	<h1>
		<?php if ( is_day() ) : /* if the daily archive is loaded */ ?>
			<?php printf( __( 'Publicaciones Diarias: <span>%s</span>' ), get_the_date() ); ?>
		<?php elseif ( is_month() ) : /* if the montly archive is loaded */ ?>
			<?php printf( __( 'Publicaciones Mensuales: <span>%s</span>' ), get_the_date('F Y') ); ?>
		<?php elseif ( is_year() ) : /* if the yearly archive is loaded */ ?>
			<?php printf( __( 'Publicaciones por Año: <span>%s</span>' ), get_the_date('Y') ); ?>
		<?php else : /* if anything else is loaded, ex. if the tags or categories template is missing this page will load */ ?>
			Archivo de Publicaciones
		<?php endif; ?>
	</h1>
		</div>
 	   
    </div>
	
	<div class="row ">
   
      <div class="col-xs-12 col-md-9 col-lg-9 ">   
<?php   if ( is_day() ) { 
	include_once("templates/loop-publicacion-single.php");
}
 else { 
 $queryhoy= array( 'post_type' => array('publicacion'),'post_status' => 'publish','posts_per_page'=> -1,'year'=>get_the_date('Y'),'monthnum'=>get_the_date('m') ); 
 query_posts( $queryhoy); 
 	include_once("templates/loop-publicaciones.php");
}
?>

      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
		 <?php dynamic_sidebar( 'sidebar' ); ?>
</div>
		</div>
	</div>



<?php   get_footer(); ?>