<?php get_header(); ?>
<div class="container" >

	<div class="row ">
   
      <div class="col-xs-12 col-md-12 col-lg-12 ">    
	<h1>
		<?php if ( is_day() ) : /* if the daily archive is loaded */ ?>
			<?php printf( __( 'Publicaciones Diarias: <span>%s</span>' ), get_the_date() ); ?>
		<?php elseif ( is_month() ) : /* if the montly archive is loaded */ ?>
			<?php printf( __( 'Publicaciones Mensuales: <span>%s</span>' ), get_the_date('F Y') ); ?>
		<?php elseif ( is_year() ) : /* if the yearly archive is loaded */ ?>
			<?php printf( __( 'Publicaciones por Año: <span>%s</span>' ), get_the_date('Y') ); ?>
		<?php else : /* if anything else is loaded, ex. if the tags or categories template is missing this page will load */ ?>
			Archivo de Remates Vigentes
		<?php endif; ?>
	</h1>
		</div>
 	   
    </div>
	
		<div class="row">

<div class="col-xs-12 col-md-12 col-lg-12 ">
	<?php 
	$args= array( 'post_type' => 'remate','order' => 'ASC','orderby'=>'term_id','post_status' => 'publish',
'posts_per_page'=> -1);
/*$args['tax_query'] = array('relation' => 'AND');
$args['tax_query'][] = array('taxonomy' => 'idpublicacion','field' => 'slug','terms' =>get_the_title(),'operator'=>'IN');*/
$loop = new WP_Query( $args );
?>    
	<?php include_once('templates/loop-remates.php'); ?>
		
	<div class="oldernewer">
		<p class="older"><?php next_posts_link('&laquo; Older Entries') ?></p>
		<p class="newer"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
	</div><!--.oldernewer-->
<?php wp_reset_query();?>
	</div>
		</div>
			
	</div>



<?php get_footer(); ?>