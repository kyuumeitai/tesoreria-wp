/******************************************************************************
	Transforms the basic Twitter Bootstrap Carousel into Fullscreen Mode
	@author Fabio Mangolini
     http://www.responsivewebmobile.com
******************************************************************************/
/* VARIABLES GLOBALES */
var $ = jQuery.noConflict();
(function ($) {
	/*  FUNCTIONS */
	$(window).on('resize', function() {
	
$('#slider-home  .item').css({'height': $(window).outerHeight()-1, 'margin': 0, 'width': $(window).outerWidth()-1});
	});
	$(document).ready(function () {
		$('#slider-home').carousel({
		interval: 5000,
		pause: "hover"}
		 
		);
		$.doTimeout(2500, function () {
			$('.repeat.go').removeClass('go');
			return true;
		});
		$.doTimeout(2520, function () {
			$('.repeat').addClass('go');
			return true;
		});
		
		/* mover menú */
		
 $('a[href*="#"]').click(function() {
	 
    moveNav(this);
  });
	
		// SLAYER
		$('#slider-home  .item').css({
			'height': $(window).outerHeight() - 1,
			'width' : $(window).outerWidth()-1
		});
		$('#slider-home .item img.first-slide, header.title-page img.first-slide, .back-image').each(function () {
			var imgSrc = $(this).attr('src');
			$(this).parent().css({
				'background': 'url(' + imgSrc + ') center center no-repeat',
				'-webkit-background-size': 'cover',
				'-moz-background-size': 'cover',
				'-o-background-size': 'cover',
				'background-size': 'cover'
			});
			$(this).remove();
		});
		
		
		
		var sections = {},
			_height = $(window).height(),
			i = 0;
		var width = $(window).width();
		// Grab positions of our sections 
		$('body').find('section').each(function () {
			sections[$(this).attr('id')] = $(this).offset().top;
		});
		var aArray = []; // create the empty aArray
		$("#menu-parallax-home li a").each(function () { // guardo #ID seccion
			if ($(this).is('[href*="#"')) {
				var cadena = $(this).attr('href');
				aArray.push(cadena);
			}
		});
		var hTxt=0;
		$("#desarrollo_web .texto-servicios .item_planes").each(function () { // guardo #ID seccion
			if ($(this).height() > hTxt) {
				hTxt=$(this).height() + 100;
			}
		});
		$("#desarrollo_web .texto-servicios .item_planes").height(hTxt);
		
		// <source src="https://player.vimeo.com/external/158148793.hd.mp4?s=8e8741dbee251d5c35a759718d4b0976fbf38b6f&profile_id=119&oauth2_token_id=57447761" type="video/mp4">
		
		$(window).scroll(function () {
		//	var stickyNavTop = $('#menu-top').offset().top;
			var scrollTop = $(window).scrollTop();
			//console.log(scrollTop + " top >" + $stickyNavTop + " scrollw y" + window.scrollY);
			// ACTIVA MENÚ STICKY
			if (scrollTop >  $(window).height()) {
				$('#menu-top').addClass('navbar-fixed-top');
			//	$('#menu-top.sticky a').find("img").remove();
			//	$('#menu-top.sticky a').html('<img src="http://www.avispaweb.com/wp-content/themes/avispaweb/images/LOGO_AVISPAWEB.php?c1=101820&amp;c2=ffcd00&l=2" alt="AvispaWeb">');
			} else {
			//	$('#menu-top.sticky a').find("img").remove();
			//	$('#menu-top.sticky a').html('<img src="http://www.avispaweb.com/wp-content/themes/avispaweb/images/LOGO_AVISPAWEB.php?c1=ffffff&amp;c2=ffcd00" alt="AvispaWeb">');
				$('#menu-top').removeClass('navbar-fixed-top ');
			}
			// SELECCIONA MENU SEGÚN SECCTION ACTIVA
			var windowPos = window.scrollY; //window.scrollTop(); // get the offset of the window from the top of page
			var windowHeight = $(window).height(); // get the height of the window
			var docHeight = $(document).height();
			for (var i = 0; i < aArray.length; i++) {
				var theID = aArray[i];
				if ($(theID).offset()) {
					var offset = $(theID).offset();
					var top = $(theID).offset().top;
					// console.log("ID" +  aArray[i] + "left: " + offset.left + ", top: " + offset.top  );
					var divPos = top - 120; // get the offset of the div from the top of page
					var divHeight = $(theID).height(); // get the height of the div in question
					if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
						$("#menu-parallax-home  li a[href='" + theID + "']").parent().addClass("active");
					} else {
						$("#menu-parallax-home  li a[href='" + theID + "']").parent().removeClass("active");
					}
				}
			}
			if (windowPos + windowHeight == docHeight) {
				if (!$("#menu-parallax-home  li:last-child a").parent().hasClass("active")) {
					var navActiveCurrent = $(".active").parent().attr("href");
					$("a[href='" + navActiveCurrent + "']").removeClass("active");
					$("#menu-parallax-home  li:last-child a").parent().addClass("active");
				}
			}
			//stickyNav();
		}); // fin scroll
		
		
		$("#contactavispa").submit(function (e) {
				e.preventDefault();
				return false;
			})
			.validate({
			errorPlacement: function(error, element) {
                $(element).before(error);
            },
            errorElement: "span",
            ignore: [],
			 rules: {
                "entry_1217272583": {
                    required: true,
                    minlength: 2
                },
               "entry_1469996039": {
                    required: true,
                    email: true
                },
               "hiddenRecaptcha": {
                required: function() {
                    if(grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            },
             messages: {
              "hiddenRecaptcha": "Verifica tu identidad, eres un robot? Test de Turing."},
				submitHandler: function (form) {
					postContactToGoogle();
				}
			});
		function postContactToGoogle() {
		$("#contactavispa .hiddenRecaptcha").remove();
			var $datos = $("#contactavispa").serialize();
			var $servicios = new Array();
			$('#contactavispa input[name="entry_860429960[]"]:checked').each(function () {
				$servicios.push($(this).val());
			
			});
			
			$.ajax({
				url: "https://docs.google.com/forms/d/e/1FAIpQLSff8F2KVnk72IzW6hxg6pgq2tA1DWhvw-StmKtvwuTsGF7W5A/formResponse",
				data: $datos + '&entry.860429960='+encodeURI($servicios),
				type: "POST",
				async: false,
				dataType: "xml",
				statusCode: {
					0: function () {
						postContactToMail();
						return false;
					}
				}
			});
		}
		function postContactToMail() {
			var $datos = $("#contactavispa").serialize();
			var $servicios = [];
			$('#contactavispa input[name="entry_860429960[]"]:checked').each(function () {
				$servicios.push($(this).val());
			});
			var url = "/wp-content/themes/avispaweb/mail.php";
			var redirect = $("#contactavispa").attr('data-redirect');
				$.ajax({
				url: url,
				data: $datos+"&entry_860429960="+encodeURI($servicios),
				type: "POST",
				async: false,
				dataType: "xml",
				statusCode: {
					200: function () {
						//alert('redirect');
						window.location.replace(redirect);
						return false;
					}
				}
			});
			return false;
			
		}
	});
	// Interactiveness now
	/* Demo Scripts for Bootstrap Carousel and Animate.css article
	* on SitePoint by Maria Antonietta Perna
	*/

function moveNav($this) {
	var $win = $(window);
	 var spacetop= parseInt(($win.width() <=780)? 100 : 20);

	if (location.pathname.replace(/^\//,'') === $this.pathname.replace(/^\//,'') && location.hostname === $this.hostname) {
      var target = $($this.hash);
	 
      target = target.length ? target : $('[name=' + $this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - spacetop
        }, 1000);
		  console.log( target.offset().top - spacetop + " top: " +target.offset().top + "- Space:"+ spacetop);
		/* $("ul.nav li").removeClass("active");
		$($this).parent().addClass("active");*/
					
        return false;
      }
    }
}
	$("#sistemas_web #video-background").html('<source src="/wp-content/themes/avispaweb/video/mlky_6.mp4" type="video/mp4">');
})(jQuery);
