<?php 
header("Content-Type: text/javascript; charset=iso-8859-1"); 
include($_SERVER["DOCUMENT_ROOT"] ."/wp-load.php");


//<script type="text/javascript">
?>
// JavaScript Document
(function ($) {
	/* CARGA IMAGENES CON EL SCROLL*/
	$("img.lazy").lazyload({
		effect: "fadeIn"
	});
	/*  FUNCTIONS */
	function resize(a) {
		/* CAMBIA ALTO SEGÚN PANTALLA */
		var $height = window.innerHeight;
		$(a).css("height", $height);
	};
	/* READY */
	jQuery(document).ready(function ($) {
		
		$.doTimeout(2500, function () {
		$('.repeat.go').removeClass('go');
			return true;
		});
		$.doTimeout(2520, function () {
			$('.repeat').addClass('go');
			return true;
		});
			
		$('.full').css({
			'width': $(window).outerWidth() - 1,
			'min-height': $(window).outerHeight() - 1
		});	
	
						$('.parallax-scrolling').find("a").bind('click', function (event) {
			/* EFECTO PARALLAX PARA NAVEGACIÓN CON EL MENÚ*/
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});
		
		<?php if(isset($_GET['page'])== 1 ){ // script solo en home ?>
		/* TAMAÑO SLIDER */
		resize("#home-slider .item");
		$('#home-slider .item').find("img").each(function () {
			var imgSrc = $(this).attr('src');
			$(this).parent().css({
				'background': 'url(' + imgSrc + ') center center no-repeat',
				'-webkit-background-size': '100% ',
				'-moz-background-size': '100%',
				'-o-background-size': '100%',
				'background-size': '100%',
				'-webkit-background-size': 'cover',
				'-moz-background-size': 'cover',
				'-o-background-size': 'cover',
				'background-size': 'cover'
			});
			$(this).remove();
		});
		<?php }  // --> FIN script solo en home ?>
		$("#contactavispa").submit(function (e) {
				e.preventDefault();
				return false;
			})
			.validate({
			errorPlacement: function(error, element) {
                $(element).before(error);
            },
            errorElement: "span",
            ignore: [],
			 rules: {
                "entry_1217272583": {
                    required: true,
                    minlength: 2
                },
               "entry_1469996039": {
                    required: true,
                    email: true
                },
               "hiddenRecaptcha": {
                required: function() {
                    if(grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            },
             messages: {
              "hiddenRecaptcha": "Verifica tu identidad, eres un robot? Test de Turing."},
				submitHandler: function (form) {
					postContactToGoogle();
				}
			});
		function postContactToGoogle() {
		$("#contactavispa .hiddenRecaptcha").remove();
			var $datos = $("#contactavispa").serialize();
			var $servicios = new Array();
			$('#contactavispa input[name="entry_860429960[]"]:checked').each(function () {
				$servicios.push($(this).val());
			
			});
			
			$.ajax({
				url: "https://docs.google.com/forms/d/e/1FAIpQLSff8F2KVnk72IzW6hxg6pgq2tA1DWhvw-StmKtvwuTsGF7W5A/formResponse",
				data: $datos + '&entry.860429960='+encodeURI($servicios),
				type: "POST",
				async: false,
				dataType: "xml",
				statusCode: {
					0: function () {
						postContactToMail();
						return false;
					}
				}
			});
		}
		function postContactToMail() {
			var $datos = $("#contactavispa").serialize();
			var $servicios = new Array();
			$('#contactavispa input[name="entry.860429960[]"]:checked').each(function () {
				$servicios.push($(this).val());
			});
			var url = "<?php echo get_stylesheet_directory_uri(); ?>/mail.php";
			var redirect = $("#contactavispa").attr('data-redirect');
			$.ajax({
				url: url,
				data: $datos + '&entry.860429960=' + encodeURI($servicios),
				type: "POST",
				dataType: "xml",
				async: false,
				success: function (html) {
					window.location.replace(redirect);
				}
			});
			return false;
		}
	}); /* FIN READY */
	
})(jQuery);
