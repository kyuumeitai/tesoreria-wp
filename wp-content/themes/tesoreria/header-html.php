<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	<title>
		<?php echo wp_title(); echo ' | '; bloginfo( 'name' );	?>
	</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel="profile" href="http://gmpg.org/xfn/11"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<?php /* Add "maximum-scale=1" to fix the Mobile Safari auto-zoom bug on orientation changes, but keep in mind that it will disable user-zooming completely. Bad for accessibility. */ ?>
	
	<?php /* The HTML5 Shim is required for older browsers, mainly older versions IE */ ?>

	<!--[if lt IE 9]>

	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

<![endif]-->

	<?php    wp_head(); ?>

</head>

<body>


<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     <a class="navbar-brand " href="<?php bloginfo( 'url' ); ?>">	
									<?php 
				$logo_primary_web = get_theme_mod( 'logo_primary_web' );
				$logo_secundary_web = get_theme_mod( 'logo_secundary_web' );	
				if ($logo_primary_web){
					echo '<img class="logo s-header__logo-img s-header__logo-img-default" src="'.esc_url( $logo_primary_web ).'" alt="'.get_bloginfo('name').'">';
				} 	
			if ($logo_secundary_web){
					echo '<img class="logo s-header__logo-img s-header__logo-img-shrink" src="'.esc_url( $logo_secundary_web ).'" alt="'.get_bloginfo('name').'">';
				} 
		
                     ?>   
		</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      <?php  	wp_nav_menu(array('menu'=> 'menu','container'=> false,
						  'echo'=> true,'items_wrap'=> '<ul class="nav navbar-nav navbar-left">%3$s</ul>',
						  'walker'=> new My_Custom_Nav_Walker()));?>
      <form class="navbar-form  navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-right">
      	<?php 
		$items='';
      	if (is_user_logged_in())
		{
			$items .= '<li class="menu-item">
						<a href="'. wp_logout_url(get_permalink()) .'" class="btn btn-primary navbar-btn btn-sm">'. __("Log Out") .'</a>
						</li>
						';
		}
		else
		{
			$items .= '<li class="menu-item btn-menu btn-login">
						<a href="'. wp_login_url(get_permalink()) .'"  class="btn btn-primary navbar-btn btn-sm">'. __("Log In") .'</a>
						</li>';
		} 

		echo $items;
		?>
       
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
