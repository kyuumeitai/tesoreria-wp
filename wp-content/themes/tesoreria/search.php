<?php get_header(); ?>

<header class="page-header title-page">
  <div class="container ">
    <div class="row "  data-appear-top-offset="-10"  data-sequence="100">
      <?php // PAGE PRINCIPAL ?>
      <div class="col-xs-12 col-md-12 col-lg-12   padding-20">
        <h1>
          <?php the_search_query(); ?>
        </h1>
      </div>
    </div>
  </div>
</header>
<div class="container page-int">
  <div class="row " >
    <?php // PAGE PRINCIPAL ?>
    <div class="col-xs-12 col-md-12 col-lg-12   padding-20">
       <div class="form-search">
       
  <div class="container-fluid">

<form class="navbar-form navbar-left" role="search" method="get" id="searchform" action="<?php bloginfo( 'url' ); ?>">
        <div class="form-group input-group-lg">
        <input value="<?php echo $_GET['s'];?>" name="s" id="s" type="text" class="form-control" placeholder="Buscar">
        </div>
        <button type="submit" class="btn btn-default" style="margin-top:0;">Buscar</button>
      </form>
  </div>
</nav>
   
      
     </div>
      <?php 
	

  /* 
BUSQUEDA POR COMUNA
 // echo $term->term_id;
     BUSCA TODAS LAS COMUNAS
 
   $wsubargs = array(
               'hierarchical' => 1,
               'show_option_none' => '',
               'hide_empty' => 0,
               'taxonomy' => 'comuna'
            );
            $wsubcats = get_categories($wsubargs);
            foreach ($wsubcats as $wsc):

 endforeach; 

     */
    $args_total=array();
    $total_post=0;
    $args_total= array( 'post_type' => 'remate', 'order' => 'asc','orderby'=>'date','posts_per_page'=>-1);  
    $args_total['meta_query'] = array('relation' => 'OR');
    $args_total['meta_query'][] = array('key' => 'data','value' => '"'.$_GET['s'].'"','compare' => 'LIKE');
  /*  $args_total['tax_query'] = array('relation' => 'AND');
    $args_total['tax_query'][] = array('taxonomy' => 'comuna','field' => 'slug','terms' =>$_GET['s'],'operator'=>'IN');*/
    print_r( $args_total);
   // $postp =  get_posts( $args_total );
    $total_post = count( get_posts( $args_total ) );
    $classbtn=($total_post ===0)? 'btn-outline-secondary':'btn-outline-primary';
    $classbadge=($total_post ===0)? 'badge-secondary':'badge-primary';       
     $loop = new WP_Query( $args_total );
    print_r($_REQUEST);
       ?>
  <table class="table table-bordered  table-hover"> 
  <thead> 
    <tr> <th >N°</th> <th>ROL</th> <th>Nombre</th> <th>Direccion</th> <th>Tasación</th><th>Fecha de Remate</th> <th>Detalle Remate</th> </tr> </thead> 
   <tbody> 
    <?php while ( $loop->have_posts() ) : $loop->the_post();
    global $post;
   
  $value = get_post_custom( $post->ID,'data' );


 $data=json_decode($value['data'][0]);
$fechaRemate=date( 'd-m-Y', strtotime($data->fechaRemate) );
 //print_r($data2);
//print_r(unserialize($data['data'][0]));
  //periodoPublicacionF

?>


    <tr> <th nowrap="nowrap" ><?php echo $data->pblRemId;?>-<?php echo $data->orden;?></th> <td ><?php echo $data->rol;?></td> <td><?php echo $data->nombreDuegno;?></td> <td><?php echo $data->direccionRol .$data->comunaJuzgado ;?></td> <td>$<?php echo  number_format((int)$data->tasacion, 0,'', '.');  ?></td><th nowrap="nowrap"><?php echo $fechaRemate;?></th><td><a href="<?php the_permalink(); ?>" class="pull-right" role="button">Ver Remate</a></td> </tr> 
   
<?php    endwhile; wp_reset_postdata(); ?> </tbody> 

</table>
      <?php    

          wp_reset_query(); 

	/*if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="post-single">
        <h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark">
          <?php the_title(); ?>
          </a></h2>
        <div class="post-excerpt">
          <?php the_excerpt();   the excerpt is loaded to help avoid duplicate content issues  ?>
        </div>
        <!--.post-excerpt--> 
      </div>
      <!--.post-single-->
      <?php endwhile; else: ?>
      <div class="no-results">
        <h2>
          <?php _e('No Results'); ?>
        </h2>
        <p>
          <?php _e('Please feel free try again!'); ?>
        </p>
        <?php get_search_form(); /* outputs the default Wordpress search form */ ?>
        <?php// if(function_exists(wp_pagenavi)): wp_pagenavi(); endif;?>
      <!--no-results-->
      <?php ?>
    </div>
  </div>
  <!-- #content --> 
</div>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
