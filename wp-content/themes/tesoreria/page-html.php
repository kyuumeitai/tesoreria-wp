<?php

// Template Name:  totalnet-html

/**

 * The template for displaying pages

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages and that

 * other "pages" on your WordPress site will use a different template.

 *

 * @package WordPress

 * @subpackage Twenty_Fifteen

 * @since Twenty Fifteen 1.0

 */



 ?>

<!doctype html>

<html class="no-js" lang="en">
<head>
<meta charset="utf-Full8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Totalnet 2015 | En construcción</title>
<link href='http://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed|Ruda' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/foundation.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/totalnet-web.css" />
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-67005233-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body>
<header class="hero">
	<h1><img src="/img/totalnet-construccion.jpg"></h1>
</header>
<footer class="footer">
  <div class="row">
    <div class="large-12 columns">
	<p class="productos-servicios">Soporte Computacional - Asesorías - Hosting - Licencias de Software - Equipos y Suministros</p>
      <p class="contacto"><a href="mailto:contacto@totalnet.cl">contacto@totalnet.cl</a> - (56)225336828</p>
    </div>
  </div>
</footer>

</body>
</html>