<?php /* 
Template Name: EXITO

*/ 
get_header(); ?>	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); $id=get_the_ID(); 
if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) {
			$image_attributes = wp_get_attachment_image_src ( get_post_thumbnail_id ( $post->ID), 'full' ) ; 
			$image= $image_attributes[0];
		}?>

<header class="title-page">
	<img class="first-slide" src="<?php echo $image; ?>" alt="<?php echo get_the_title();?>">

<canvas id="demo-canvas"></canvas>
<div class="container page">

    <div class="row  animatedParent" >

    <?php // PAGE PRINCIPAL ?>

      <div class="col-xs-12 col-md-12 col-lg-12 animated flipInY padding-20">      

					<h1><?php the_title(); ?></h1>
     </div>

     </div>

 </div>

 </header>

    <div class="container page">

    <div class="row ">

    <?php // PAGE PRINCIPAL ?>

      <div class="col-xs-12 col-md-12 col-lg-12   padding-20">    

 
<?php the_content(); ?>
    


</div> 

    </div>

    </div>

	<?php endwhile; ?>

<?php get_footer(); ?>

