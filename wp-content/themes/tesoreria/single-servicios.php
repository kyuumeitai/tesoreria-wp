<?php 
/* 
Template Name: PAGE-LANDING
Template Post Type:  page, servicios
*/ 
get_header('landing');
global $post;
$custom = get_post_custom( $post->ID ); ?>
<header id="landing">
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-4 col-md-4 col-lg-4">
				
				<div class="navbar-header pull-left ">
						<a class="navbar-brand " href="<?php bloginfo( 'url' ); ?>">	
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/LOGO_AVISPAWEB.php?c1=101820&amp;c2=ffcd00&l=0" alt="<?php echo bloginfo( 'name' );?>" >
					</a></div></div>
			<div class="col-xs-12  col-sm-8 col-md-8 col-lg-8">
				<div class="pull-right text-right"><h1><span>Contrata tu Plan</span><br> Diseño y Desarrollo de Sitios Web<div class="hide"><?php the_title();?></div>
				</h1></div>
			</div>
		</div>
	</div>
	
</header>

<section id="page-landing" class="container-fluid"><?php the_post_thumbnail('full',array('class'=>'back-image hide')); ?>
<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12 col-md-12 col-lg-12">
				<h3 class="pull-right text-right"><?php echo $custom['slogan1_plan_web'][0];?><br><span><?php echo $custom['slogan2_plan_web'][0];?></span></h3>

			</div>
		</div>
			<div class="row">
			<div class="col-xs-12  col-sm-8 col-md-8 col-lg-8">
			<div class="item_planes">
		<span class="plan">Plan Sitio Web</span>
		<h3><?php the_title();?></h3>
		<span class="plan-small">Desde</span><br>
		<div class="price">
		<i class="fa fa-dollar" aria-hidden="true"></i><?php echo $custom['valor_plan_web'][0];?> <span class="small-iva" >IVA Incluido</span></div>
	<div class="cotizar"><a class="btn  center-block " data-attr="<?php echo $custom['valor_plan_web'][0];?>" href="#contacto">Contrata tu plan</a></div>
				
				
					</div>
		</div>	
			<div class="col-xs-12  col-sm-3 col-md-3 col-lg-3 col-md-offset-1">
			<?php include_once('inc/formulario-landing-servicios.php'); ?> 
		</div>	
		</div>	
		</div>
			
	
</section>
<section id="body-landing">
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-9 col-md-9 col-lg-9">
			<?php echo $post->post_content; ?>
				<div class="como-funciona">
<div class="row"><div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
  <h3 class="listatit-avispate">¡Así Funciona!</h3>
	</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><h5 class="listatit-avispate"><span class="corchete left">{</span>Caracteristicas de <br>nuestros servicios<span class="corchete right">}</span></h5></div>
</div>
<div class=" lista-avispate">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<div class="bg"><img src="http://www.avispaweb.com/wp-content/uploads/2017/09/idea.png" alt="idea"/>
  <div class="text-center" ><span>1</span>
    <h4>Elije tu plan</h4>
    Selecciona según tú presupuesto</div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
  <div class="bg"><img src="http://www.avispaweb.com/wp-content/uploads/2017/09/asesoria.png" alt="asesoria"/>
    <div class="text-center" ><span>2</span>
      <h4>Asesosamiento</h4>
      Nos enfocamos en crear soluciones según tus necesidades y modelo de negocios</div>
  </div>
</div>
</div>
	
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<div class="bg">
<img src="http://www.avispaweb.com/wp-content/uploads/2017/09/estrategia.png" alt="estrategia"/>
<div class="text-center" ><span>3</span>
  <h4>Estrategía</h4>
  Incluye
  Configuraciones básicas para SEO, Optimización On-Site, Implementación Google Analytics, Redireccionamientos</div>
</div></div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
  <div class="bg"><img src="http://www.avispaweb.com/wp-content/uploads/2017/09/design.png" alt="design"/>
    <div class="text-center" ><span>4</span>
      <h4>Diseño</h4>
      Nuestros Diseños son 100% Responsivos, Únicos y Exclusivos para nuestros clientes. </div>
  </div>
</div></div>
<div class="row ">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
  <div class="bg"><img src="http://www.avispaweb.com/wp-content/uploads/2017/09/implementacion.png" alt="implementacion"/>
    <div class="text-center" ><span>5</span>
      <h4>Implementación</h4>
      Se crea la programación, efectos y estilos para las distintas secciones</div>
  </div></div>
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="bg"><img src="http://www.avispaweb.com/wp-content/uploads/2017/09/final.png" alt="final"/>
      <div class="text-center" ><span>6</span>
        <h4>Puesta en marcha</h4>
        Finalizamos con la publicación del Sitio Web.</div>
    </div>
  </div>
</div>
</div>
	
		</div>
			</div>
		</div>
	

	</div>
</section>
	

<?php get_footer(); ?>

