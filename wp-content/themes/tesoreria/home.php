<?php get_header();

/* PUBLICACIÓN DEL DIA */
	
 $sqllastpublish="SELECT post_date FROM `tesoreria_posts`   WHERE post_type='publicacion' AND post_status='publish' ORDER BY `tesoreria_posts`.`post_date` DESC LIMIT 1";
 $lastDatepublish=$wpdb->get_var($sqllastpublish);
 $searchPublicacion=explode("-",date('Y-m-d', strtotime( $lastDatepublish) ));
wp_reset_postdata();
 $queryhoy= array( 'post_type' => array('publicacion'),'post_status' => 'publish','year'=>$searchPublicacion[0],'monthnum'=>$searchPublicacion[1],'day'=>$searchPublicacion[2],'posts_per_page'=> -1); 
 query_posts( $queryhoy); ?> 
 <div class="container">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
 <?php include_once("templates/loop-publicacion-single.php"); ?>
</div></div></div>
<?php wp_reset_postdata(); ?><?php get_footer(); ?>