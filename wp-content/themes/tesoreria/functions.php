<?php
//error_reporting(E_ERROR | E_WARNING );
error_reporting(E_ERROR );
//error_reporting(E_ALL );
// ELIMINO POR NO USO EN WORDPRESS	

remove_action('wp_head', 'wp_generator');  
remove_action('wp_head', 'index_rel_link'); 
remove_action('wp_head', 'start_post_rel_link', 10, 0); 
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 
remove_action('wp_head', 'parent_post_rel_link', 10, 0); 
remove_action('wp_head', 'rsd_link'); 
remove_action('wp_head', 'wlwmanifest_link'); 
remove_action('wp_head', 'feed_links', 2); 
remove_action('wp_head', 'feed_links_extra', 3); 
//remove_action('wp_head', 'rel_canonical'); 
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 ); 
//remove_action('wp_head', 'wp_enqueue_scripts', 1); REMUEVE SCRIPT DEL HEADER
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' ); 

// scripts

add_action('init', 'opciones_post');
add_action( 'wp_enqueue_scripts', 'theme_styles_avispaweb' );
//add_action( 'wp_head', 'wp_enqueue_scripts' );


//add_action('get_header', 'remove_admin_login_header');
//add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-thumbnails', array( 'post', 'page','slider' ) );
add_action('wp_footer', 'wp_enqueue_scripts', 5); 


	// adds Post Format support

	// learn more: http://codex.wordpress.org/Post_Formats

	// add_theme_support( 'post-formats', array( 'aside', 'gallery','link','image','quote','status','video','audio','chat' ) );

add_filter('the_generator', 'wb_remove_version');// removes the WordPress version from your header for security


function remove_admin_login_header() {

   // remove_action('wp_head', '_admin_bar_bump_cb');

}



/*------------------------------------------------------------*/
/*	Logo Login WP
/*------------------------------------------------------------*/

function my_login_logo() {

    $logo_primary_web = get_theme_mod( 'logo_primary_web' );
             
                if ($logo_primary_web){
                 //   echo '<img class="logo s-header__logo-img s-header__logo-img-default" src="'.esc_url( $logo_primary_web ).'" alt="'.get_bloginfo('name').'">';
                 ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo esc_url( $logo_primary_web ); ?>);
           
            -webkit-background-size: 100%;
            background-size: 100%;
        }
    </style>
<?php }
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );


/*------------------------------------------------------------*/
/*	Registrar Menus
/*------------------------------------------------------------*/

function register_my_menus() {
  register_nav_menus(
    array(
      'menu-principal' => __( 'Menu Principal' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );


/*------------------------------------------------------------*/
/*	AGREGAR CARACTERISTICAS A SECCIONS
/*------------------------------------------------------------*/

function opciones_post(){

add_post_type_support('post', 'excerpt'); // extracto en post

add_post_type_support('page', 'excerpt'); // extractos para páginas

}

function wb_remove_version() {

		return '<!-- Theme Creado por MARCELA GODOY con Boostrap -->';

}


function theme_styles_avispaweb() {
/*------------------------------------------------------------*/
/*	LLAMADO A TODOS LOS CSS
/*------------------------------------------------------------*/

	/****** FONTS ******/
wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Droid+Sans:400,700|Open+Sans:300,400,600,700|Raleway:100,200,300,400,500,600,700|Roboto:100,300,400,500|Dancing+Script' );

/****** ESTILOS ESTRUCTURA ******/
wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css' );
wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri().'/plugins/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css' );
wp_enqueue_style( 'style-avispa', get_template_directory_uri().'/style.css' );
wp_enqueue_style( 'font-awesome',  get_template_directory_uri().'/plugins/font-awesome-4.7.0/css/font-awesome.min.css' );

/****** HOME ******/

wp_enqueue_style( 'merriweather-css', 'https://fonts.googleapis.com/css?family=Merriweather%3A300%2C400%2C400i%2C700%2C900%7COpen+Sans%3A300%2C400&amp;ver=4.8' );
wp_enqueue_style( 'header-lt', 'http://s2.latercera.com/wp-content/themes/gnuble/css/header.css?ver=5.6.1' );
wp_enqueue_style( 'footer-lt', 'http://s2.latercera.com/wp-content/themes/gnuble/css/footer.css?ver=4.8' );
wp_enqueue_style( 'main-lt', 'http://s2.latercera.com/wp-content/themes/gnuble/css/main.css?ver=6.9.1' );

/*   <link id="merriweather-css" type="text/css"
    href="https://fonts.googleapis.com/css?family=Merriweather%3A300%2C400%2C400i%2C700%2C900%7COpen+Sans%3A300%2C400&amp;ver=4.8"
    rel="stylesheet" media="all"> <link id="header-css" type="text/css" href="http://s2.latercera.com/wp-content/themes/gnuble/css/header.css?ver=5.6.1"
    rel="stylesheet" media="all">
  <link id="footer-css" type="text/css" href="http://s2.latercera.com/wp-content/themes/gnuble/css/footer.css?ver=4.8"
    rel="stylesheet" media="all">
  <link id="main-css" type="text/css" href="http://s2.latercera.com/wp-content/themes/gnuble/css/main.css?ver=6.9.1"
    rel="stylesheet" media="all"><link id="footer-css" type="text/css" href="http://s2.latercera.com/wp-content/themes/gnuble/css/footer.css?ver=4.8"
    rel="stylesheet" media="all">*/	
	
/****** PLUGINS ******/
	
//wp_enqueue_style( 'animate-it', get_template_directory_uri() . '/plugins/css3-animate-it-master/css/animations.css' );

/****** POST TYPES ******/
/*
if(is_singular( 'servicios' )){
	
		wp_enqueue_style( 'font-gilroy',  get_template_directory_uri().'/css/font-gilroy.css' );
		wp_enqueue_style( 'landing', get_template_directory_uri().'/css/landing-servicios.css' );
}
*/
}



function theme_script_avispaweb() {
/*------------------------------------------------------------*/
/*	LLAMADO A TODOS LOS JQUERY
/*------------------------------------------------------------*/
	
/*------------- JS GENERALES ------------------------------*/ 
wp_deregister_script( 'jquery' );
wp_register_script( 'jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js", false, null );
wp_enqueue_script( 'jquery' );
wp_enqueue_script('jquery-ui');	
wp_enqueue_script( 'bootstrap',get_template_directory_uri() .'/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js', array('jquery'), '3.3.7', false);
wp_enqueue_script( 'modernizr',get_template_directory_uri() . '/js/modernizr.js', array(), '', false);

//wp_enqueue_script( 'jquery-validate', 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js', array('jquery'),'1.13.0' , true );
//wp_enqueue_script( 'jquery-form', get_template_directory_uri() . '/js/forms.min.js', array('jquery'), '1.0.0', true );

	
/*------------- JS ESPECIALES SEGÚN PAGINA ------------------------------*/ 


/****** HOME ******/

/****** otros ******/

wp_enqueue_script( 'jquery-avispa', get_template_directory_uri() . '/js/scripts-avispa.js', array('jquery'), '1.0.0', true );

	
	/* EFECTO */

}


if ( function_exists('register_sidebar') )
register_sidebar(array('name'=>'Buscar Tribunales','id'=>'tribunal',
'before_widget' => '<div class="texto-home" >',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>'
));
if ( function_exists('register_sidebar') )
register_sidebar(array('name'=>'Buscar Comunas','id'=>'comunas',
'before_widget' => '<div class="texto-home" >',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>'
));
if ( function_exists('register_sidebar') )
register_sidebar(array('name'=>'Sidebar','id'=>'sidebar',
'before_widget' => '<div class="texto-home" >',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>'
));

/*------------------------------------------------------------*/
/*	ARCHIVOS EXTERNOS: NAVWALKER - POST_TYPES - SLIDER - ETC
/*------------------------------------------------------------*/

require_once('inc/wp_bootstrap_navwalker.php');


add_filter('widget_text','do_shortcode');
/*
 * Disable the WP REST API
 */
add_filter( 'rest_authentication_errors', 'disable_rest_api' );
function disable_rest_api( $access ) {
  return new WP_Error( 'rest_disabled', __( 'The REST API on this site has been disabled.' ), array( 'status' => rest_authorization_required_code() ) );
}

?>