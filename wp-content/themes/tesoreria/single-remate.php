<?php get_header(); ?>
<?php
if ( have_posts() )
	while ( have_posts() ): the_post();
    global $post;
   
  $value = get_post_custom( $post->ID,'data' );

 $data=json_decode($value['data'][0]);
$fechaRemate=date( 'd-m-Y', strtotime($data->fechaRemate) );
$table_posts= $wpdb->prefix . "posts";
 $k=(string)date( 'Y-m-d', strtotime($data->fechaPrimeraPublicacion));
 $sql="SELECT ID FROM $table_posts WHERE post_title='$k' AND post_type='publicacion'";
  $links=$wpdb->get_var($sql);
//postidPUB =  get_page_by_path( (string) $data->pblRemId) ) ;
//echo $sql;
?>

    <div class="container page">

    <div class="row ">

    <?php // PAGE PRINCIPAL ?>

      <div class="col-xs-12 col-md-12 col-lg-12   padding-20">    


<div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title">Remate N° <span class="label label-primary"><?php echo $data->orden;?></span> - Publicación N° <span class="label label-primary"><?php echo $data->pblRemId;?></span> </div>
            </div>
            <div class="panel-body">
            <table class="table table-bordered  table-hover"> 
       
            <tbody>
         <tr>
                 <th colspan="2" class="text-center">
                <h1 class="m-top-0"><span class="label label-default"> Remate N° <?php echo $data->orden;?></span></h1></th>  
                  
                     </tr>
              <tr>
                 <th  class="text-center">Fecha de Primera Publicación
               <a href="<?php echo get_the_permalink($links); ?>" title="Ver Publicación"> <h2 class="m-top-0"><span class="label label-default"> <?php the_time('d'); ?> <?php the_time('F'); ?>, <?php the_time('Y'); ?></span></h2></a></th>  
                  <th  class="text-center">Fecha de Segunda Publicación
                    <?php if($data->marcaSegundaPublicación==true) { ?>
                <h2 class="m-top-0"><span class="label label-default"><?php echo $data->fechaActualizacion;?></span></h2></th>
                 <?php } ?>    </tr>
              
        
                
                <tr>
                 <th colspan="2" class="text-center">
                <h3 class="m-top-0">Antecedentes de la propiedad  </span></h3></th>  
           </tr>
            <tr><th>ROL</th> <td><?php echo $data->rol;?></td> </tr>
            <tr><th>Propietario</th> <td><?php echo $data->nombreDuegno;?></td> </tr>
            <tr><th>Dirección</th> <td><?php echo $data->direccionRol;?></td> </tr>
            <tr><th>Comuna</th> <td><?php echo $data->comunaJuzgado;?></td> </tr>
            <tr><th>Región</th> <td><?php //echo $data->rol;?></td> </tr>
              <tr>
                 <th colspan="2" class="text-center">
                <h3 class="m-top-0">Antecedentes del Remate  </span></h3></th>  
           </tr>
            <tr><th>Tesoreria</th> <td><?php echo $data->rol;?></td> </tr>
            <tr><th>Juzgado</th> <td><?php echo $data->nombreJuzgado;?></td> </tr>
            <tr><th>Dirección Juzgado</th> <td><?php echo $data->direccionJuzgado;?>, <?php echo $data->comunaJuzgado;?></td> </tr>
            <tr><th>Fecha Remate</th> <td><?php echo date( 'Y-m-d H:i:s', strtotime($data->fechaRemate) )?></td> </tr>
            <tr><th>Expediente Judicial Nº</th> <td><?php echo $data->nroExpJud;?></td> </tr>
            <tr><th>Expediente Administrativo Nº</th> <td><?php echo $data->identificacionExpedienteAdm;?></td> </tr>
            <tr><th>Tasación</th> <td>$<?php echo  number_format((int)$data->tasacion, 0,'', '.');  ?></td> </tr>
            <tr><th>Tipo Deuda</th> <td><?php echo $data->tipoDeuda;?></td> </tr>
            <tr><th>Tipo Impuesto</th> <td><?php echo $data->tipoImpuesto;?></td> </tr>
              <tr>
              
                <td colspan="2">
                  <?php //print_r($value); ?>
<div class="btn-group " role="group" aria-label="...">
  <a href="#" class=" btn btn-default btn-sm" role="button">Ver PDF</a></div>
       
                 </td>
              
              </tr>
            </tbody>
          </table>
            </div>
          </div>




</div> 

    </div>

    </div>

    <?php
//print_r($data);
//</pre><pre>
     endwhile; ?>

<?php get_footer(); ?>