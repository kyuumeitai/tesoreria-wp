<?php get_header(); ?>
<?php
if ( have_posts() )
	while ( have_posts() ): the_post();
$id = get_the_ID();
?>

<header class="title-page">
	<div class="container ">
		<div class="row  ">
			<?php // PAGE PRINCIPAL ?>
			<div class="col-xs-12 col-md-12 col-lg-12 animated flipInY padding-20">
				<h1>
					<?php the_title(); ?>
				</h1>
				<!--#pageMeta-->
				<!--#post-# .post-->

			</div>

		</div>

	</div>

</header>

    <div class="container page">

    <div class="row ">

    <?php // PAGE PRINCIPAL ?>

      <div class="col-xs-12 col-md-12 col-lg-12   padding-20">    

 

<?php the_content(); ?>

     


</div> 

    </div>

    </div><?php endwhile; ?>

<?php get_footer(); ?>